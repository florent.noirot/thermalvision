import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardManagerComponent } from './dashboard-manager/dashboard-manager.component';
import { SharedModule } from '../shared/shared.module';
import { ReportModule } from '../report/report.module';


@NgModule({
  declarations: [
    DashboardManagerComponent,
    
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReportModule
  ],
  exports: [
    DashboardManagerComponent
  ]
})
export class DashboardModule { }
