import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashboardService } from '../../_services/dashboard.service';
import { VideowallService } from '../../_services/videowall.service';
import { Subscription } from 'rxjs';
import { UserService } from '../../_services/user.service';


@Component({ selector: 'dashboard-manager', templateUrl: './dashboard-manager.component.html', styleUrls: ['./dashboard-manager.component.css'] })
export class DashboardManagerComponent implements OnInit, OnDestroy {

  public chartsConfig = [
    {'label': 'alarms', 'type': 'bar', 'key': 'alarms', 'aggregation': '', 'title': 'Allarmi Gionalieri', 'restricted':true, 'class': 'big', 'dimensions': {'height':270, 'width':'100%'}},// WIDGET 1
    {'label': 'kpi', 'type': 'none', 'key': 'kpi', 'aggregation': 'num_impianti', 'title': 'Numero impianti', 'restricted':true, 'class': 'small', 'dimensions': {'height':270, 'width':'100%'}},
    {'label': 'tasks', 'type': 'line', 'key': 'tasks', 'aggregation': '', 'title': 'Andamento Task in Stato Aperto', 'restricted':true, 'class': 'big', 'dimensions': {'height':270, 'width':'100%'}},
    {'label': 'kpi', 'type': 'none', 'key': 'kpi', 'aggregation': 'device_raggiungibili', 'title': 'Device Raggiungibili', 'restricted':true, 'class': 'small', 'dimensions': {'height':270, 'width':'100%'}},
    {'label': 'kpi', 'type': 'none', 'key': 'kpi', 'aggregation': 'task_chiusi', 'title': 'Task Chiusi', 'restricted':true, 'class': 'small', 'dimensions': {'height':270, 'width':'100%'}},
  ];
  public darkMode = 'light';
  public darkMode$: Subscription;
  
  public data = null;
  public data$: Subscription;

  public tenant = null;
  public tenant$: Subscription;

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(
    public dashboardService: DashboardService,
    public videowallService: VideowallService,
    public userService: UserService, 

  ) { }

  async ngOnInit() {
    this.tenant$ = this.videowallService.tenant$.subscribe(async tenant => {
      this.tenant =  tenant;
      this.data = null;
      this.getReport(tenant);
    });
    
    this.data$ = this.dashboardService.data$.subscribe(async data => {
      this.data = data;
      console.log( this.tenant, ' : GET DASHBOARD DATA => ', this.data);
    });

    this.darkMode$ = this.userService.darkMode$.subscribe(async darkMode => {
      this.darkMode = darkMode;
      // console.log('DARKMODE CHANGE => ', darkMode);
    });
  }


  public getReport(tenant){
    this.dashboardService.getReport(tenant);
  }

  // ##########################################
  // ############## DESTROYER ##############
  // ##########################################
  async ngOnDestroy() {
    if(this.darkMode$){this.darkMode$.unsubscribe();}
    if(this.tenant$){this.tenant$.unsubscribe();}
    if(this.data$){this.data$.unsubscribe();}
  }
}
