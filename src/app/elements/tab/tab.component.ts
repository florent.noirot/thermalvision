import { Component, ElementRef, ViewChild, Input, AfterViewInit, EventEmitter, Output} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as config from '../../../assets/data/config.js';

@Component({ selector: 'tab', templateUrl: './tab.component.html', styleUrls: ['./tab.component.css'] })

export class TabComponent implements AfterViewInit{
  @Input('data') data;
  @Input('name') name;
  @Input('config') config;
  @Input('disabled') disabled;
  @Output('formAnagrafica') formAnagrafica: FormGroup;
  @Output() result = new EventEmitter();
  public fields;
  public originalData;
  public bChanged = false;
  public selectData = {task_type: config.TASK_TYPES};
  public profileForm: any;
  varGroup = {};
  public viewForm = false;
  public objGroup = {};
  public msgError = {};
  public formAnagraficaValid: boolean = false;


  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(private formBuilder: FormBuilder) {
  }

  ngAfterViewInit() {
    this.fields = config.COMMESSA_DETAIL_FIELDS[this.name];
    if (this.data) {
      this.originalData = JSON.parse(JSON.stringify(this.data));
    }
    // console.log('TAB AVI => ', this.name, ' => ', this.data, ' => ', this.fields);

    if (this.fields) {
      this.setGroups();
      this.formAnagrafica = this.formBuilder.group(
        this.objGroup
      );
    }
    this.viewForm = true;
  }

  // ##########################################
  // ##### GET AND EMIT CHANGES TO PARENT #####
  // ##########################################
  public handleChanges(field, value){
    if(field == 'data_all' || field == 'data_val' ){
      const field2 = (field == 'data_all' ?'data_allarme' : 'data_validazione');
      let temp = new Date(value);
      this.data[field] = value;
      this.data[field2] = temp.toISOString();
    } 
    
    // console.log('^^^^^ HANDLECHANGE TAB 2 : ', field, '|', value, ' => ', this.data[field], ' => ', this.data);
    this.bChanged = (JSON.stringify(this.data) == JSON.stringify(this.originalData) ? false : true);
    this.emitChanges();
  }

  // ##########################################
  // ############ DROPDOWN CHANGE #############
  // ##########################################
  onChange(field, value) {
    // console.log(field, ', ', value)
    this.handleChanges(field, value);
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.formAnagrafica.controls[controlName].hasError(errorName);
  }

  // ##########################################
  // ######### EMIT CHANGES TO PARENT  #######
  // ##########################################
  public emitChanges() {
    this.result.emit({ data: this.data, bChanged: this.bChanged });
    this.formAnagraficaValid = this.formAnagrafica.valid;
  }


  // ##########################################
  // ############### RESET DATA ###############
  // ##########################################
  public resetData(){
    this.data = JSON.parse(JSON.stringify(this.originalData));
    this.bChanged = (JSON.stringify(this.data) == JSON.stringify(this.originalData) ? false : true);
    this.emitChanges();
  }

  setGroups() {
    Object.entries(this.fields).forEach(([key, el], index) => {
      let arrValidator = this.createArrValidator(el);
      this.objGroup[el[0]] = ['', arrValidator];
    });
  }

  createArrValidator(el) {
    let arr = [];
    if (el[6].error.required) {
      arr.push(Validators.required);
    }
    if (el[6].error.minlength) {
      arr.push(Validators.minLength(el[6].error.minlength));
    }
    if (el[6].error.maxlength) {
      arr.push(Validators.maxLength(el[6].error.maxlength));
    }
    return arr;
  }

  // ##########################################
  // ###### TRACKBY FN: OLVED FOCUS ISSUE #####
  // ##########################################
  trackByFn(index, item) {
    return index;  
  }

  isValid() {
    if (this.formAnagrafica) {
      if (this.formAnagrafica.valid) {
        return true;
      }
      else {
        Object.keys(this.formAnagrafica.controls).forEach(field => {
          const control = this.formAnagrafica.get(field);
          control.markAsTouched({ onlySelf: true });
        });
        return false;
      }
    }
  }


}



