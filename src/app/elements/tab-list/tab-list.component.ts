import { Component, OnInit, SimpleChanges, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as config from '../../../assets/data/config.js';

@Component({ selector: 'tab-list', templateUrl: './tab-list.component.html', styleUrls: ['./tab-list.component.css']})
export class TabListComponent implements AfterViewInit {

  @Input('data') data;
  @Input('name') name;
  @Output() result = new EventEmitter();
  @Output('formTabList') formTabList: FormGroup;
  public fields;
  public fields_K_V;
  public originalData;
  public bChanged = false;
  public objGroup = {};
  public viewForm = false;
  public formTabListValid: boolean = false;

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(private formBuilder: FormBuilder) {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.isValid();
  }

  ngAfterViewInit() {
    this.fields = config.COMMESSA_DETAIL_FIELDS[this.name];
    if (this.data) {
      this.originalData = JSON.parse(JSON.stringify(this.data));
    }
    // console.log('TAB AVI => ', this.name, ' => ', this.data, ' => ', this.fields);
  
    if (this.fields) {
      this.setGroups();
      this.formTabList = this.formBuilder.group(
        this.objGroup
      );
    }
    this.viewForm = true;
  }

  // ##########################################
  // ##### GET AND EMIT CHANGES TO PARENT #####
  // ##########################################
  public handleChanges(field, value){
    this.bChanged = (JSON.stringify(this.data) == JSON.stringify(this.originalData) ? false : true);
    this.emitChanges();
  }

  public emitChanges(){
    this.result.emit({data: this.data, bChanged: this.bChanged});
    this.formTabListValid = this.formTabList.valid;
  }

  public resetData(){
    this.data = JSON.parse(JSON.stringify(this.originalData));
    this.bChanged = (JSON.stringify(this.data) == JSON.stringify(this.originalData) ? false : true);
    this.emitChanges();
  }


  // ##########################################
  // ###### TRACKBY FN: OLVED FOCUS ISSUE #####
  // ##########################################
  trackByFn(index, item) {
    return index;  
  }


  public addCounter() {
    this.data = [...this.data, Object.keys(this.data[0])];
  }

  public delCounter(i) {
    this.data.splice(i, 1);
  }

  setGroups() {
    Object.entries(this.fields).forEach(([key, el], index) => {
      let arrValidator = this.createArrValidator(el);
      this.objGroup[el[0]] = ['', arrValidator];
    });
  }

  createArrValidator(el) {
    let arr = [];
    if (el[6].error.required) {
      arr.push(Validators.required);
    }
    if (el[6].error.minlength) {
      arr.push(Validators.minLength(el[6].error.minlength));
    }
    if (el[6].error.maxlength) {
      arr.push(Validators.maxLength(el[6].error.maxlength));
    }
    return arr;
  }
  
  public hasError = (controlName: string, errorName: string) => {
    return this.formTabList.controls[controlName].hasError(errorName);
  }

  isValid() {
    if (this.formTabList) {
      if (this.formTabList.valid) {
        return true;
      }
      else {
        Object.keys(this.formTabList.controls).forEach(field => {
          const control = this.formTabList.get(field);
          control.markAsTouched({ onlySelf: true });
        });
        return false;
      }
    }
  }
  
}


