import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner/spinner.component';
import { MiniProfileComponent } from './mini-profile/mini-profile.component';
import { TabComponent } from './tab/tab.component';
import { TableListComponent } from './table/table.component';
import { WindowComponent } from './window/window.component';
import { MaterialModule } from '../shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabListComponent } from './tab-list/tab-list.component';
import { IconComponent } from './icon/icon.component';
import { DarkmodeComponent } from './darkmode/darkmode.component';


@NgModule({
  declarations: [
  
    SpinnerComponent,
    MiniProfileComponent,
    TabComponent,
    TableListComponent,
    WindowComponent,
    TabListComponent,
    IconComponent,
    DarkmodeComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  exports: [
    SpinnerComponent,
    MiniProfileComponent,
    TabComponent,
    TableListComponent,
    WindowComponent,
    TabListComponent,
    IconComponent,
    DarkmodeComponent
  ]
})
export class ElementsModule { }
