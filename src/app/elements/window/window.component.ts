import { Component, OnInit } from '@angular/core';

@Component({selector: 'window', templateUrl: './window.component.html', styleUrls: ['./window.component.css'] })
export class WindowComponent implements OnInit {

  public data ={
    impianti: {
      sede0: "sede0",
      indirizzo0: "indirizzo0"
    },
    ripartitori: {
      sede1: "sede1",
      indirizzo1: "indirizzo1"
    },
    rif_impianti: {
      sede2: "sede2",
      indirizzo2: "indirizzo2"
    }
  }
  constructor() { }

  ngOnInit(): void {
  }

}
