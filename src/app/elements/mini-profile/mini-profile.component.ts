import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'mini-profile',
  templateUrl: './mini-profile.component.html',
  styleUrls: ['./mini-profile.component.css']
})
export class MiniProfileComponent implements OnInit {

  public loginStatus$: Subscription;
  public loginStatus = false;
  public profile = null

  constructor(
    public authenticationService: AuthenticationService, 
  ) { }

  ngOnInit(): void {
      //LOGIN STATUS
      this.loginStatus$ = this.authenticationService.loginStatus$.subscribe(data => {
        this.loginStatus = data;
        this.profile = JSON.parse(localStorage.getItem("profile"));
        // console.log('MINI PROFILE => ', this.profile);

      });
  }

  logout(){
    console.log('LOGGING OUT');
    this.authenticationService.logout();

  }

}
