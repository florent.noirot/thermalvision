import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { SVGInjector } from '@tanem/svg-injector'

@Component({selector: 'icon', templateUrl: './icon.component.html', styleUrls: ['./icon.component.css'] })
export class IconComponent implements AfterViewInit {

  @Input("id") id;
  @Input("color") color;
  @Input("type") type;

  constructor() { }

  ngAfterViewInit(): void {
    this.generateIcon();
  }

  public generateIcon(){
    
    //CREATE SVG INSIDE DIV
    let el = document.getElementById('containerIcon_' + this.type + '_' + this.id); 
    let svg = document.createElement('div'); 
    svg.className =  'icon_' + this.id;
    svg.dataset.src = "assets/images/icons/pin.svg";
    el.appendChild(svg); 

    var elementsToInject = document.querySelectorAll('.icon_' + this.id);          
    const that = this;
    SVGInjector(elementsToInject, { 
      afterAll(elementsLoaded) { }, 
      afterEach(err, svg) { if (err) {throw err;} }, 
      beforeEach(svg) { 
        svg.setAttribute('fill', that.color);
        svg.setAttribute('height', '15px');
        svg.setAttribute('width', '15px');
      }, cacheRequests: false, evalScripts: 'once', httpRequestWithCredentials: false, renumerateIRIElements: false,
    })
  }


}
