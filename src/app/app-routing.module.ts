import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './core/home/home.component';
import { VideowallManagerComponent } from './videowall/videowall-manager/videowall-manager.component';
import { DashboardManagerComponent } from './dashboard/dashboard-manager/dashboard-manager.component';
import { AnagraficaManagerComponent } from './anagrafica/anagrafica-manager/anagrafica-manager.component';
import { TasksManagerComponent } from './tasks/tasks-manager/tasks-manager.component';
import { ReportManagerComponent } from './report/report-manager/report-manager.component';
import { AlarmsManagerComponent } from './alarms/alarms-manager/alarms-manager.component';
import { OktaAuthGuard, OktaCallbackComponent } from '@okta/okta-angular';
import { PrivacyCheckComponent } from './core/privacy-check/privacy-check.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  // { path: 'login/callback', component: OktaCallbackComponent},
  { path: 'login/callback', component: PrivacyCheckComponent},
  { path: 'dashboard', component: DashboardManagerComponent, canActivate: [ OktaAuthGuard ]},
  { path: 'videowall', component: VideowallManagerComponent, canActivate: [ OktaAuthGuard ]},
  { path: 'tasks', component: TasksManagerComponent, canActivate: [ OktaAuthGuard ]},
  { path: 'notifications', component: AlarmsManagerComponent, canActivate: [ OktaAuthGuard ]},
  { path: 'anagrafica', component: AnagraficaManagerComponent, canActivate: [ OktaAuthGuard ]},
  { path: 'report', component: ReportManagerComponent, canActivate: [ OktaAuthGuard ]},
];
// { path: 'privacy', component: Privacy2Component, canActivate: [ OktaAuthGuard ] },

@NgModule({imports: [RouterModule.forRoot(routes)], exports: [RouterModule]})
export class AppRoutingModule { }
