import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';

const roles = [
  {
      "role_id": "direttore_area",
      "permission_type": "R",
      "function_id": "anagrafica"
  },
  {
      "role_id": "direttore_area",
      "permission_type": "R",
      "function_id": "dashboard"
  },
  {
      "role_id": "direttore_area",
      "permission_type": "R",
      "function_id": "report"
  },
  {
      "role_id": "direttore_area",
      "permission_type": "R",
      "function_id": "tasks"
  },
  {
      "role_id": "direttore_area",
      "permission_type": "R",
      "function_id": "videowall"
  }
];

@Injectable({providedIn: 'root'})
export class UserService {

  public loading = new BehaviorSubject<any>(false);
  public loading$ = this.loading.asObservable();    
  public darkMode = new BehaviorSubject<any>('light');
  public darkMode$ = this.darkMode.asObservable();    
  public menuVisible = new BehaviorSubject<any>(true);
  public menuVisible$ = this.menuVisible.asObservable();  
  public personalVisible = new BehaviorSubject<any>(false);
  public personalVisible$ = this.personalVisible.asObservable();  
  public userFunctionalities = new BehaviorSubject<any>([]);
  public userFunctionalities$ = this.userFunctionalities.asObservable();  

  constructor(
    private http: HttpClient
  ) {
    // const darkModeLS = localStorage.getItem('darkMode');
    // if(darkModeLS){
      // console.log('INIT DARKMODE => ', darkModeLS);
      // this.setDarkMode(darkModeLS);
    // }
  }
  
  //##########################################
  //############### DARKMODE #################
  //##########################################
  setDarkMode(value){
    this.darkMode.next(value);
    // localStorage.setItem('darkMode', value)
  }

  //##########################################
  //############### SPINNER #################
  //##########################################
  setloading(value){this.loading.next(value);}

  //##########################################
  //############ MENU VISIBILITY #############
  //##########################################
  setMenuVisibility() {this.menuVisible.next(!this.menuVisible.value);}

  //##########################################
  //############ MENU PERSONAL #############
  //##########################################
  setPersonalVisibility() {this.personalVisible.next(!this.personalVisible.value);}


  //################################## GET USER ROLES  ##################################
  //################################## GET USER ROLES  ##################################
  public getMenuFunctionalities(role) {

    const url = environment.url + environment.code + '/roleauthorization?role_id='+ role;
    const options = {headers: new HttpHeaders({ 'Auth' : `Bearer ${localStorage.getItem('token')}`,'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET', 'Accept': '*/*', 'Content-Type' : 'application/json' })}

    // console.log('GETUSERROLE 1 => ', url, ' => ', options);
    return new Promise((resolve, reject) => {
      this.http.get(url, options).subscribe((data) => {
        let role = data;
        // console.log('GETUSERROLE 2 => ', role);
        this.userFunctionalities.next(role);
        resolve(role) 
        return role
      },err => { return [];});
    })    
  
  
  }
  
}
