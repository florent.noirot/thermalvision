
export function generateRandom(len) {
  var p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  return [...Array(len)].reduce(a => a + p[~~(Math.random() * p.length)], '');
}

export function formatTimestamp(ts, format) {
  const months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
  const monthsName = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Decembre']
  const day = new Date(ts * 1000);
  let result = "";
  // const result = (dateTimeStr.split(", ")[1]).split(":").join("/");
  // console.log(ts, ' => ', day);

switch(format){
  case 'DMYHMS':
    result =
    (day.getDate() <= 9 ? '0' + day.getDate() : day.getDate()) + ' ' +
    monthsName[day.getMonth()] + ' ' +
    day.getFullYear().toString() + ' ' +
    (day.getHours() <= 9 ? '0' + day.getHours() : day.getHours()) + ':' +
    (day.getMinutes() <= 9 ? '0' + day.getMinutes() : day.getMinutes()) + ':' + 
    (day.getSeconds() <= 9 ? '0' + day.getSeconds() : day.getSeconds());  
  break;

  default:
    result =
    (day.getDate() <= 9 ? '0' + day.getDate() : day.getDate()) + '/' +
    months[day.getMonth()] + ' ' +
    // day.getFullYear().toString() + ' ' +
    (day.getHours() <= 9 ? '0' + day.getHours() : day.getHours()) + ':' +
    (day.getMinutes() <= 9 ? '0' + day.getMinutes() : day.getMinutes());
  break;
}


  return result;
}

export function formatDateIt(date) {
  let Y = date.substring(0, 4);
  let m = date.substring(5, 7);
  let d = date.substring(8, 10);
  let h = date.substring(11, 16);
  
  return d+'/'+m+'/'+Y+' '+h;
}




  // ########################################
  // ############## SET SEARCH PERIOD ##############
  // ########################################
  export function getTimeframe(){
    let result = {};
    let temp = new Date();
    temp.setHours(temp.getHours() - 48); 
    temp.setDate(temp.getDate() -2)
    temp.setHours(0,0)
    result['from'] = temp.toISOString();
    
    temp = new Date();
    temp.setHours(23,59);
    result['to'] = temp.toISOString();

    return result;
  }
