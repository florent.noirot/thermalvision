import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlarmsService {

  public alarms = new BehaviorSubject<any>([]);
  public alarms$ = this.alarms.asObservable();    
  
  constructor(
    private http: HttpClient,
) { }


  //################################## GET ALARMS  ##################################
  //################################## GET ALARMS  ##################################
  public getAlarms(tenant) { 

    // const url =  "/assets/data/alarms.json";
    const url = environment.url + environment.code + '/getalarm?tenant='+ tenant;
    const options = { 
      headers: new HttpHeaders(
        { 
          'Access-Control-Allow-Origin': '*', 
          'Access-Control-Allow-Methods': 'GET', 
          'Accept': '*/*', 
          'Content-Type' : 'application/json', 
          'Auth' : `Bearer ${localStorage.getItem('token')}`,
        }
      )
    }

    return new Promise((resolve, reject) => {

        this.http.get(url, options).subscribe(data => {
          let alarms = [];
          const impianti = JSON.parse(localStorage.getItem('impianti')); 
          const temp = JSON.parse(JSON.stringify(data));
          temp.forEach(element => {
            const indexImpianto = this.getElement(impianti, element["wbs"]+'-'+element["rif_impianto"]);
            const impianto = impianti[indexImpianto];
            // console.log(' - ', element);
            // element['id_amministratore'] = impianto['properties']['id_amministratore'];
            // element['sede'] = impianto['properties']['sede'];
            // element['indirizzo'] = impianto['properties']['indirizzo'];
            // element['comune'] = impianto['properties']['comune'];
            alarms.push(element);
          });

          console.log('###### GETALARMS 4 => ', alarms);
          this.alarms.next(alarms);
          resolve(alarms);
          return alarms
        },err => { 
          console.log('GETALARMS ERROR => ', err);
          return [];
      });
    })    

  }


  //################################## POST ALARMS ##################################
  //################################## POST ALARMS ##################################
  public getElement(data, param) { 
    let res = 0;
    data.forEach((element, index) => {
      if(element["wbs"]+'-'+element["rif_impianto"] == param){
        res = index;
      }
    });
    return res;
  }


}
