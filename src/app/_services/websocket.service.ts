import { Injectable } from "@angular/core";
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { environment } from '../../environments/environment';
// import * as Rx from "rxjs/Rx";

@Injectable()
export class WebsocketService {

  // public subject: WebSocketSubject<MessageEvent>;
  public subject;

  //################################## CONSTRUCTOR ##################################
  //################################## CONSTRUCTOR ##################################
  constructor() {}


  //################################## CONNECT ##################################
  //################################## CONNECT ##################################
  // public connect(url): WebSocketSubject<MessageEvent> {  
  //   try{
  //     if (!this.subject) {
  //       this.subject = this.create(url);
  //       console.log("- Successfully connected: " + url);
  //     }
  //     return this.subject;
    
  //   } catch(e) {
  //     console.log('ERROR - CONNECTING - TO WEBSOCKET SERVER => ', e);
  //   }
  // }

  
  //################################## CREATE ##################################
  //################################## CREATE ##################################
  // public create(url): WebSocketSubject<MessageEvent> {
    
  //   try{
  //     let ws = new WebSocket(url);
  //     let observable = Rx.Observable.create(
  //       (obs: Rx.Observer<MessageEvent>) => {
  //         ws.onmessage = obs.next.bind(obs);
  //         ws.onerror = obs.error.bind(obs);
  //         ws.onclose = obs.complete.bind(obs);
  //         return ws.close.bind(ws);
  //       });
  
  //     let observer = {
  //       next: (data: Object) => {
  //         if (ws.readyState === WebSocket.OPEN) {
  //           // console.log("WS 1 => ", ws.readyState, " === ", WebSocket.OPEN, " => ", ws.readyState === WebSocket.OPEN, " => ", data);
  //           ws.send(JSON.stringify(data));
  //         }
  //       }
  //     };
  //     return Rx.Subject.create(observer, observable);

  //   } catch(e) {
  //     console.log('ERROR - CREATE - WEBSOCKET SERVER => ', e);
  //   }
  // }

  //################################## CONNECT ##################################
  //################################## CONNECT ##################################
  public connect(url): WebSocketSubject<MessageEvent> {  
    try{
      if (!this.subject) {
        this.subject = this.create(url);
      }
      return this.subject;
    
    } catch(e) {
      console.log('ERROR - CONNECTING - TO WEBSOCKET SERVER => ', e);
    }
  }

  //################################## CREATE ##################################
  //################################## CREATE ##################################
  public create(url){
    
    const url2 = url + '?x-api-key=' + environment.key_ws;
    try{
      const webSocketSubject = webSocket<string>({
        url: url2
      });
      webSocketSubject.next('OPEN CONNECTION => ' + url2);
      webSocketSubject.next('OPEN CONNECTION A');
      console.log('OPEN CONNECTION1 => ', url2);
      webSocketSubject.subscribe({
        error: (err) => {
          console.log('$$$$ ERROR OCCURED $$$$ : ', err)
        },
        next: (msg) => {
          // console.log('---> MESSAGED RECEIVED ---> : ', msg);
        },
        complete: () => {
          console.log('XXXX CONNECTION CLOSED XXXX');
          webSocketSubject.next('REOPEN CONNECTION');
          console.log('---> REOPENING CONNECTION --->');
          webSocketSubject.next('CONNECTION SUCCESSFUL');
        }
      });
     
      this.maintainingWSOpen(webSocketSubject);

      return webSocketSubject;

    } catch(e) {
      console.log('ERROR - CREATE - WEBSOCKET SERVER => ', e);
    }
  }


  public maintainingWSOpen(webSocketSubject){
    
    setInterval(() => {
      
      webSocketSubject.next('MAINTAINING CONNECTION');
        // console.log('++++++++ : ', new Date(), ' ++++++++++');
    }, 300000);
    
  }



}


