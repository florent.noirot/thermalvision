import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { resolve } from 'dns';


@Injectable({providedIn: 'root'})
export class DashboardService {

  public data = new BehaviorSubject<any>(null);
  public data$ = this.data.asObservable();    

  constructor(
    private http: HttpClient,
  ) { }


  //################################## GET REPORT DATA ##################################
  //################################## GET REPORT DATA ##################################
  public getReport(tenant) {

    const type = ['alarms', 'tasks', 'kpi', 'tasks2', 'alarms2']
    const url =  "/assets/data/report_alarms.json";
    const url2 =  "/assets/data/report_tasks.json";
    const url3 =  "/assets/data/report_kpi.json";
    const url4 =  "/assets/data/report_tasks2.json";
    const url5 =  "/assets/data/report_alarms2.json";
    
    // const url = environment.url + environment.code + '/getsuivi?id_impianto=';
    // const url2 = environment.url + environment.code + '/getsuivi?id_impianto=';
    const options = { 
      headers: new HttpHeaders(
        { 
          'Access-Control-Allow-Origin': '*', 
          'Access-Control-Allow-Methods': 'GET', 
          'Accept': '*/*', 
          'Content-Type' : 'application/json', 
          'Auth' : `Bearer ${localStorage.getItem('token')}` 
        }
      )
    }


    return Promise.all([
      this.http.get(url, options),
      this.http.get(url2, options),
      this.http.get(url3, options),
      this.http.get(url4, options),
      this.http.get(url5, options),
    ]).then(data => {
        let result = {};
          data.map((obs, index) => {
            obs.subscribe(val => {
              // console.log('--- OBS', index, ' => ', val);
              result[type[index]] = val;
            })
          })
          
          setTimeout(() => {this.data.next(result);}, 2000);


        // return result; // RETURN WORKS
      
      });




  }



}
