import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { OktaAuthStateService, OKTA_AUTH } from '@okta/okta-angular';
import { AuthState, OktaAuth } from '@okta/okta-auth-js';
import { UserService } from '../_services/user.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
// import { PrivacyComponent } from '../core/privacy/privacy.component';

@Injectable({ providedIn: 'root' })

export class AuthenticationService {

  public loginStatus = new BehaviorSubject<any>(false);
  public loginStatus$ = this.loginStatus.asObservable();  
  
  constructor(
    public oktaAuthState: OktaAuthStateService, 
    @Inject(OKTA_AUTH) private oktaAuth: OktaAuth,
    private router: Router, 
    public http: HttpClient, 
    public userService: UserService,
    public dialog: MatDialog
  ) { 
    this.getLoginStatus();
  }

  //##########################################
  //############## LOGIN STATUS ##############
  //##########################################
  setLoginStatus(status) {
    // localStorage.setItem('bLogin', status);
    // this.loginStatus.next(status);
  }

  getLoginStatus() {
    this.loginStatus.next(false);
    this.oktaAuthState.authState$.subscribe(async res => {
      if(res.isAuthenticated) {
        const userClaims = await this.oktaAuth.getUser();
        const accessToken = this.oktaAuth.getAccessToken();
        const refreshToken = this.oktaAuth.getRefreshToken();

        const userRole = JSON.parse(localStorage.getItem('role'));
        // console.log('### OKTA ISAUTHENTICATED => ', userRole);
       
        localStorage.setItem('profile', JSON.stringify(userClaims));
        localStorage.setItem('email', userClaims['email']);
        localStorage.setItem('token', accessToken);
        localStorage.setItem('refresh_token', refreshToken);
        this.askPrivacy();

        this.userService.getMenuFunctionalities(userRole['role'][0]);
        
      }
      else {
        // console.log('AUTHENTICATION SERVICE 2A => ', isAuthenticated);
        // localStorage.clear();
        // this.router.navigate(['']);
      }
      this.loginStatus.next(res.isAuthenticated); 
    });
  }




















  // // ################################### HANDLES PRIVACY ################################### 
  // // ################################### HANDLES PRIVACY ################################### 
  async askPrivacy(){
    // const privacy = localStorage.getItem('privacy');

    // const temp = await this.getUserPrivacy();
    // // console.log('ASK PRIVACY 0 => ', temp);
    // const email = localStorage.getItem('email');
    // let privacy2 = (Object.values(temp)[0] ? Object.values(temp)[0]['last_action'] : 'none');
    // // console.log('ASK PRIVACY 1 => ', email, ' => ', typeof privacy2, ' => ', privacy2);

    // if(privacy2 != 'accept') {
    //   const dialogRef = this.dialog.open(PrivacyComponent, {
    //     data: { type: 'request' }
    //   });
      
    //   dialogRef.afterClosed().subscribe(result => {
    //     if(result == true){
    //       localStorage.setItem('privacy', 'true');
    //       this.setUserPrivacy(email, 'accept');
          this.goNavigation();
    //     } else { this.logout();}
    //   });
    // } else { 
    //   setTimeout(() => { this.goNavigation();}, 1500);
    // }
  }


  // //############################# SAVES USER PRIVACY IN DATABASE #############################
  // //############################# SAVES USER PRIVACY IN DATABASE #############################
  // setUserPrivacy(email, action) {
  //   let body = { "Items": [{"user_id": email, "action": action }] }
  //   const url = environment.url + environment.code + '/userprivacypost';
  //   const options = {
  //     headers: new HttpHeaders({'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET', 'Accept': '*/*', 'Content-Type' : 'application/json', 'Auth' : `Bearer ${localStorage.getItem('token')}`})
  //   }

  //   return new Promise((resolve, reject) => {
  //     this.http.post(url, body, options).subscribe((data) => {
  //       console.log('SETPRIVACY 2 => ', data);
  //       resolve(data);
  //     }, err => { return null;});
  //   })    
  // }

  // //############################# GET USER PRIVACY HISTORY #############################
  // //############################# GET USER PRIVACY HISTORY #############################
  // getUserPrivacy() : Promise<Object> {
  //   // const url = environment.url + environment.code + '/userprivacy?user_id=' + localStorage.getItem('email');
  //   const url = environment.url + environment.code + '/userprivacy?user_id=' + localStorage.getItem('email');
  //   const options = {headers: new HttpHeaders({'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET', 'Accept': '*/*', 'Content-Type' : 'application/json', 'Auth' : `Bearer ${localStorage.getItem('token')}`})};

  //   // console.log('GETPRIVACY 1 => ', url, ' => ', localStorage.getItem('token'));
  //   return new Promise <Object> ((resolve, reject) => {
  //     this.http.get(url, options).subscribe( (data) => {
  //       // console.log('GETPRIVACY 2 => ', data);
  //       resolve(data);
  //     }, err => {return null;});
  //   });   

  // }

  //############################# LOGOUT #############################
  //############################# LOGOUT #############################
  async logout() {
    this.oktaAuth.signOut();
    localStorage.clear();
    // console.log('**** OKTA LOGOUT ****');
  }

  // ################################### GOHOME ################################### 
  // ################################### GOHOME ################################### 
  goNavigation(){

    let url = window.location.href.replace(environment.url + environment.code + '/', '');
    url = window.location.href.replace('http://localhost:4200' + '/', '');
    url = (url.includes('login/callback') ? 'videowall' : url);
    
    // console.log('GONAVIGATION => ', url);
    this.router.navigate([url]);
  }

  // ################################### GOLOGIN ################################### 
  // ################################### GOLOGIN ################################### 
  goHome(){
    this.router.navigate(['/']);
  }




}

