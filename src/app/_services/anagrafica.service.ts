import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class AnagraficaService {

  public commesse = new BehaviorSubject<any>([]);
  public commesse$ = this.commesse.asObservable();    

  constructor(
    private http: HttpClient
  ) { }

  //################################## GET ENTIRE LIST COMMESSE  ##################################
  //################################## GET ENTIRE LIST COMMESSE  ##################################
  public getCommesse(tenant) {

    // const url =  "/assets/data/commessa.json";
    const url = environment.url + environment.code + '/ana?tenant='+ tenant['city'];
    const options = { 
      headers: new HttpHeaders(
        { 
          'Access-Control-Allow-Origin': '*', 
          'Access-Control-Allow-Methods': 'GET', 
          'Accept': '*/*', 
          'Content-Type' : 'application/json', 
          'Auth' : `Bearer ${localStorage.getItem('token')}` 
        }
      )
    }

    console.log('GETCOMMESSE 1 => ', url, ' => ', options);
    return new Promise((resolve, reject) => {
      this.http.get(url, options).subscribe((data) => {
        let commesse = JSON.parse(JSON.stringify(data));
        
        console.log('GETCOMMESSE 2 => ', commesse);
        commesse = commesse.map(element => {
          element = {
            ...element, 
            ...element['impianti'][0], 
            // ...{'nome_amministratore': element['amministratori']['nome']}, 
            // ...{'nome_squadra': element['squadre']['nome']}
            ...{'nome_squadra': {}},
            ...{'nome_amministratore': {}}, 
          };
          // delete element['amministratori'];
          delete element['squadre']; 
          return element;
        });
        

        resolve(commesse);
        return commesse
      },err => { 
          return [];
      });
    })    

  }




  //################################## GET DETAILS OF SPECIFIC COMMESSA ##################################
  //################################## GET DETAILS OF SPECIFIC COMMESSA ##################################
  public getCommessaDetails(id) {

    const url = environment.url + environment.code + '/anadetail?wbs=' + id;
    // const url =  "/assets/data/commessaDetails.json";

    const options = { 
      headers: new HttpHeaders(
        { 
          'Access-Control-Allow-Origin': '*', 
          'Access-Control-Allow-Methods': 'GET', 
          'Accept': '*/*', 
          'Content-Type' : 'application/json', 
          'Auth' : `Bearer ${localStorage.getItem('token')}` 
        }
      )
    }

    return new Promise((resolve, reject) => {
      this.http.get(url, options).subscribe((data) => {
        console.log('RESULTS COMMESSA DETAILS => ', data);
        let res = this.formatData(data);
        resolve(res);
      },err => { 
          return [];
      });
    })    

  }


  public formatData(data){
    let res = {};
    const tabs = [
      {key: 'amministratori', label: 'amministratori'}, 
      {key: 'contatori', label: 'contatori'}, 
      {key: 'centrali_termiche', label: 'centrali_termiche'},
      {key: 'ripartitori', label: 'ripartitori'},
      {key: 'riferimenti_tecnici', label: 'riferimenti_tecnici'},
      {key: 'servizi_impianti', label: 'servizi_impianti'},
      {key: 'contatermie', label: 'contatermie'},
      {key: 'reperibilita_impianti', label: 'reperibilita'},
      {key: 'eventi_impianti', label: 'eventi_impianti'},
    ];

    res = JSON.parse(JSON.stringify(data));
    // res['commessa'] = res['commessa'];
    res['squadra'] = res['commessa']['squadre'];
    // res['amministratore'] = res['commessa']['amministratori'];
    delete res['commessa']['amministratori'];
    delete res['commessa']['squadre'];

    tabs.forEach(tab => {
      res[tab.label] = [];
      data['impianti'].forEach(element => {
        res[tab.label] = res[tab.label].concat(element[tab.key]);
        delete element[tab.key];
      });
    });
    
    console.log('CCC => ', res);
    return res;
  }

}

