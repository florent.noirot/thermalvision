import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { AlarmsService } from './alarms.service';
import { ToastrService } from 'ngx-toastr';

const Feature = { 
  "type": "Feature", 
  "geometry": { "type": "Point", "coordinates": [0, 0] },
  "properties": {} 
}

@Injectable({providedIn: 'root'})
export class VideowallService {

  public impianti = new BehaviorSubject<any>({features: []});
  public impianti$ = this.impianti.asObservable();    
  public filteredImpianti = new BehaviorSubject<any>({features: []});
  public filteredImpianti$ = this.filteredImpianti.asObservable();    
  public tenant = new BehaviorSubject<any>({ "city": "lombardia_est", "label": "Lombardia Est", "coordinates": [45.548828, 10.119829], "zoom": 6});
  public tenant$ = this.tenant.asObservable();  
  public loading = new BehaviorSubject<any>(false);
  public loading$ = this.loading.asObservable();    
  public centerPoint = new BehaviorSubject<any>({coordinates: [45.548828, 10.119829], zoom: 6}); // FOR CENTERING ON SELECTED IMPIANTO
  public centerPoint$ = this.centerPoint.asObservable();   


  constructor(
    private http: HttpClient,
    public as: AlarmsService,
    public toastr: ToastrService
  ) {
    
    let tenant = { "city": "lombardia_est", "label": "Lombardia Est", "coordinates": [45.548828, 10.119829], "zoom": 6};
    if(localStorage.getItem('tenant')){ tenant = JSON.parse(localStorage.getItem('tenant'));}
    this.setTenant(tenant);
  }


  //##########################################
  //############### TENANT #################
  //##########################################
  setTenant(value){
    localStorage.setItem('tenant', JSON.stringify(value));
    this.tenant.next(value);
    // this.centerPoint.next({coordinates: value['coordinates'], zoom: 6});
  }

  //################################## GET ENTIRE LIST COMMESSE  ##################################
  //################################## GET ENTIRE LIST COMMESSE  ##################################
  public getImpianti(tenant) {

    this.loading.next(true);

    const url = environment.url + environment.code + '/ana?tenant='+ tenant['city'];
    const url2 = environment.url + environment.code + '/getalarm?tenant='+ tenant['city'];
    const options = {headers: new HttpHeaders({ 'Auth' : `Bearer ${localStorage.getItem('token')}`,'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET', 'Accept': '*/*', 'Content-Type' : 'application/json' })}

    // console.log('GETIMPIANTI 1 => ', url, ' => ', options);
    // return new Promise((resolve, reject) => {
    //   this.http.get(url, options).subscribe((data) => {
    //     let impianti = JSON.parse(JSON.stringify(data));
    //     impianti = this.transformGeoJSON(impianti);
    //     localStorage.setItem('impianti', JSON.stringify(impianti.features));
    //     this.impianti.next(impianti);
    //     this.filteredImpianti.next(impianti);
    //     resolve(impianti);
    //     return impianti
    //   },err => { 
    //       return [];
    //   });
    // })    
  

    return Promise.all([
      this.http.get(url, options),
      this.http.get(url2, options),
    ]).then((values) => {
        values.map((element, index) => {
        element.subscribe(data => {
          //GETTING ALARMS
          if(index == 1){
            // console.log('QQQQQ VAL ALARMS ', index,' => ', data);
            this.as.alarms.next(data);
            setTimeout(() => {
              this.loading.next(false);
            }, 3000);

          }

          //GETTING IMPIANTO
          if(index == 0){
            setTimeout(() => {
              let impianti = JSON.parse(JSON.stringify(data));
              const alarms = this.as.alarms.value;
              impianti = this.transformGeoJSON(impianti);
              localStorage.setItem('impianti', JSON.stringify(impianti.features));
              this.impianti.next(impianti);
              impianti = this.checkAlarm(alarms, impianti);
              this.filteredImpianti.next(impianti);
            }, 1000);
          }
        })
      })


    });
  }


  // TRASNFORM GET IMPIANTI INTO PROPER GEOJSON OBJECT
  public transformGeoJSON(data) {
    let res = {
      "type": "FeatureCollection", 
      "features": [] 
    }

    for (const key in data){
      let tempCommessa = data[key];
      let commessa = JSON.parse(JSON.stringify(tempCommessa));
      let squadre = commessa['squadre'];
      delete commessa['impianti'];
      delete commessa['squadre'];

      for (const keyImpianto in tempCommessa['impianti']){
        let impianto = JSON.parse(JSON.stringify(tempCommessa['impianti'][keyImpianto]));

        // console.log('++ IMPIANTO => ', impianto['wbs'] + impianto['rif_impianto']);
        impianto['commessa'] = commessa;
        impianto['squadre'] = squadre;
        const indirizzo = this.capitalize(impianto['sede']) + ', ' + this.capitalize(impianto['indirizzo']) + ', ' + this.capitalize(impianto['comune']); 
        let element = JSON.parse(JSON.stringify(Feature));
        element.geometry.coordinates = [parseFloat(impianto.lat), parseFloat(impianto.lon)];
        element.properties = {...element.properties, ...impianto, ...impianto['squadre'], ...{indirizzo: indirizzo}};
        // console.log('+++ => ', element);
        res.features.push(element);
      }
    }
    // console.log('TRANSFORMED => ', res);
    return res;
  }

  // FILTER IMPIANTI - SUBSCRIBED RESULTS ON MAPS COMPONENT
  public filterImpianti(filterText){
    const impianti = this.impianti.value;
    let filteredImpianti = JSON.parse(JSON.stringify(impianti));
    // console.log('+++ Filtering : ', filterText, ' => ', filteredImpianti);
    if (filterText.replace(" ", "").length > 0){
      const tempFiltered = impianti.features.filter(obj => {
        let result =  false;
        for (const key in obj.properties){
          if(obj.properties[key]){

            if(obj.properties[key].toString().toLowerCase().includes(filterText)){
              // console.log('-  : ', filterText, ' => ', obj.properties);
              result = true;
            }
          }
        }
        return result;
      })
      filteredImpianti.features = tempFiltered;    
    }
    filteredImpianti = this.checkAlarm(this.as.alarms.value, filteredImpianti);
    // console.log('FILTEREDIMPIANTI NEXT : FILTERIMPIANTi');
    this.filteredImpianti.next(filteredImpianti);
  }


  //CHECK IF IMPIANTO HAS ALARM ON
  public checkAlarm(alarms, impianti) {
    let res = JSON.parse(JSON.stringify(impianti));
    const alarms2 = this.listToHash(alarms, 'id_impianto');
    // console.log('## IMPIANTI 2 => ', res['features']);
    // console.log('## ALARMS 2 => ', alarms2);
    for (const key in res['features']){
      const id_impianto = res['features'][key]['properties']['wbs'] + res['features'][key]['properties']['rif_impianto'];
      // console.log('- ', key, ' : ', id_impianto);
      // if(id_impianto === '48434001'){console.log('++ IMPIANTO => ', key, ' : ', id_impianto);}
      if(alarms2[id_impianto]){
        if(!alarms2[id_impianto]['status_history']){
          console.log('ALARM EXISTS FOR : ', res['features'][key]['properties']['wbs'] + res['features'][key]['properties']['rif_impianto'], ' => ', alarms2[res['features'][key]['properties']['wbs'] + res['features'][key]['properties']['rif_impianto']]);
          res['features'][key]['properties']['squadre']['colore_original'] = JSON.parse(JSON.stringify(res['features'][key]['properties']['squadre']['colore']));
          res['features'][key]['properties']['squadre']['colore'] = "#ff0000";
          // console.log('+++ : ', res['features'][key]['properties']['squadre']);
        }
      }
    }
    return res;
  }


  //################################## GET ENTIRE LIST COMMESSE  ##################################
  //################################## GET ENTIRE LIST COMMESSE  ##################################
  public getSuivi(id) {

    // const url =  "/assets/data/impianti.json";
    const url = environment.url + environment.code + '/getsuivi?id_impianto='+ id;
    const options = { 
      headers: new HttpHeaders(
        { 
          'Access-Control-Allow-Origin': '*', 
          'Access-Control-Allow-Methods': 'GET', 
          'Accept': '*/*', 
          'Content-Type' : 'application/json', 
          'Auth' : `Bearer ${localStorage.getItem('token')}` 
        }
      )
    }

    // console.log('GETSUIVI 1 => ', url, ' => ', options);
    return new Promise((resolve, reject) => {
      this.http.get(url, options).subscribe((data) => {
        let suivi = JSON.parse(JSON.stringify(data));
        
        resolve(suivi);
        return suivi
      },err => { 
          return {};
      });
    })    
  }


  //################################## GET ENTIRE LIST COMMESSE  ##################################
  //################################## GET ENTIRE LIST COMMESSE  ##################################
  public getTemps(id) {

    // const url =  "/assets/data/impianti.json";
    const url = environment.url + environment.code + '/temperatures?id_impianto='+ id;
    const options = { 
      headers: new HttpHeaders(
        { 
          'Access-Control-Allow-Origin': '*', 
          'Access-Control-Allow-Methods': 'GET', 
          'Accept': '*/*', 
          'Content-Type' : 'application/json', 
          'Auth' : `Bearer ${localStorage.getItem('token')}` 
        }
      )
    }

    return new Promise((resolve, reject) => {
      this.http.get(url, options).subscribe((data) => {
        let temps = JSON.parse(JSON.stringify(data));
        console.log('GETTEMPS 1 => ', temps);
        resolve(temps);
        return temps
      },err => { 
          return {};
      });
    })    
  }


  //################################## POST ALARMS ##################################
  //################################## POST ALARMS ##################################
  public postAlarms(data) { 

    // const url =  "/assets/data/alarms.json";
    const url = environment.url + environment.code + '/updatealarm';
    const options = { 
      headers: new HttpHeaders(
        { 
          'Access-Control-Allow-Origin': '*', 
          'Access-Control-Allow-Methods': 'GET', 
          'Accept': '*/*', 
          'Content-Type' : 'application/json', 
          'Auth' : `Bearer ${localStorage.getItem('token')}`,
        }
      )
    }

    console.log('SETALARMS 1 => ', url, ' => ', data);
    return new Promise((resolve, reject) => {
        this.http.patch(url, data, options).subscribe((data2) => {
        console.log('SETALARMS 2 => ', data);
        this.updateStatusImpianti(data['id_impianto'], false);
        this.as.getAlarms(data['tenant']);
        this.toastr.success((data['status'] == 'managed' ? 'Task creato per allarme' : 'Allarme silenziato'));
        resolve(data2);
      },err => {
        this.toastr.error("Errore durante l'aggiornamento dell'allarme : ", JSON.stringify(err));
        return [];
      });
    })    
  }


  //################################## LIST TO HASH  ##################################
  //################################## LIST TO HASH  ##################################
  public listToHash(data, chiave){
    try{
      let res = {};
      data.forEach(element => {
        if(!element['status_history']){
          res[element[chiave]] = element;
        }
      });
      return res;
    }
    catch{return ""}
  }


  //################################## CAPITALIZE  ##################################
  //################################## CAPITALIZE  ##################################
  public updateStatusImpianti(id, status){
    //UPDATES IMPIANTI COLOR
    let impianti = this.filteredImpianti.value;
    console.log('UPDATING STATUS : ', id, ' : ', status);


    for (const key in impianti['features']){
      if (impianti['features'][key]['properties']['wbs'] + impianti['features'][key]['properties']['rif_impianto'] == id){
        switch(status){
          case true:
            if(!impianti['features'][key]['properties']['squadre']['colore_original']){
              console.log('UPDATING STATUS TRUE : ', id);
              impianti['features'][key]['properties']['squadre']['colore_original'] = JSON.parse(JSON.stringify(impianti['features'][key]['properties']['squadre']['colore']));
            }
            impianti['features'][key]['properties']['squadre']['colore'] = "#ff0000";
          break;
          case false:
            // console.log('SILENCING COLOR 6 => ', key, ' => ', impianti['features'][key]['properties']['squadre']['colore_original']);
              console.log('UPDATING STATUS FALSE : ', id);
              impianti['features'][key]['properties']['squadre']['colore'] = JSON.parse(JSON.stringify(impianti['features'][key]['properties']['squadre']['colore_original']));
          break;

        }
      }
    }
    this.filteredImpianti.next(impianti);
  }
        

  //################################## CAPITALIZE  ##################################
  //################################## CAPITALIZE  ##################################
  public capitalize(str){
    try{
      let res = str.charAt(0).toUpperCase() + str.slice(1);
      return res;
    }
    catch{
      return ""
    }
  }


}
