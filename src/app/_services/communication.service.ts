import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs/Rx";
import { WebsocketService } from "./websocket.service";
import { BehaviorSubject } from 'rxjs';
import { VideowallService } from "../_services/videowall.service";
import { environment } from '../../environments/environment';
import { split2 } from "@okta/okta-auth-js";

//const url ="wss://wss-thermalvision.noprod.aws.engie.it/noprod";
const url = environment.url_ws;

@Injectable()
export class CommunicationService {

  public messages;
  public alerts = new BehaviorSubject<any>(null);
  public alerts$ = this.alerts.asObservable();  
  public tasks = new BehaviorSubject<any>(null);
  public tasks$ = this.tasks.asObservable();  
  public temps = new BehaviorSubject<any>(null);
  public temps$ = this.temps.asObservable();  
  public tempsCarousel = new BehaviorSubject<any>(null);
  public tempsCarousel$ = this.tempsCarousel.asObservable();  

  //##########################################
  //############# CONSTRUCTOR ################
  //##########################################
  constructor(
    public wsService: WebsocketService,
    public vs: VideowallService
  ) {
    // this.messages = wsService.connect(url).map(
    //   (response: MessageEvent)  => {

    //     console.log('COMMUNICATION CONST => ', response.data);
    //     let data = JSON.parse(response.data);
    //     return data;
    //   }
    // );

    this.messages = wsService.connect(url);
  }

  //##########################################
  //############### ALERTS #################
  //##########################################
  setAlerts(value){
    this.alerts.next(value);
    // UPDATES IMPIANTO COLOR
    console.log('UPDATING ICONE COLORS => ', value, ' => ', value['id'].split(';')[1]);
    let id = (value['status'] == 'inactive' ? value['id'].split(';')[1] : value['wbs'] + value['rif_impianto']) ;

    this.vs.updateStatusImpianti(id, (value['status'] == 'inactive' ? false : true ));

  }

  //##########################################
  //############### TASKS #################
  //##########################################
  setTasks(value){this.tasks.next(value);}

  //##########################################
  //############### TEMPS #################
  //##########################################
  setTemps(value){this.temps.next(value);}
  setTempsCarousel(value){this.tempsCarousel.next(value);}




}