import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { VideowallService } from './videowall.service';
import { environment } from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class TasksService {

  public tasks = new BehaviorSubject<any>(null);
  public tasks$ = this.tasks.asObservable(); 
  
  public tasksTV = new BehaviorSubject<any>(null);
  public tasksTV$ = this.tasksTV.asObservable(); 

  constructor(
    private http: HttpClient,
    public vs: VideowallService,
  ) { }

  //################################## GET ENTIRE LIST TASKS  ##################################
  //################################## GET ENTIRE LIST TASKS  ##################################
  public getTasks(id, from, to) { // GET ENTIRE LIST OR FOR SPECIFIC IMPIANTO

    const url = environment.url + environment.code + '/gettask?id_impianto=' + id + '&opendate_from=' + from + '&opendate_to=' + to;
    const options = { 
      headers: new HttpHeaders(
        { 
          'Access-Control-Allow-Origin': '*', 
          'Access-Control-Allow-Methods': 'GET', 
          'Accept': '*/*', 
          'Content-Type' : 'application/json', 
          'Auth' : `Bearer ${localStorage.getItem('token')}`,
        }
      )
    }

    console.log('GETTASKS 1 => ', url, ' => ', options);
    return new Promise((resolve, reject) => {
      this.http.get(url, options).subscribe((data) => {
        let tasks = JSON.parse(JSON.stringify(data));
        console.log('GETTASKS 2 => ', tasks);
        resolve(tasks);
        this.tasks.next(tasks);
      },err => { 
          return [];
      });
    })    

  }

  public getTasksTV(tenant) { // GET ENTIRE LIST OR FOR SPECIFIC IMPIANTO

    const url = environment.url + environment.code + '/gettasktv?tenant=' + tenant;
    const options = { 
      headers: new HttpHeaders(
        { 
          'Access-Control-Allow-Origin': '*', 
          'Access-Control-Allow-Methods': 'GET', 
          'Accept': '*/*', 
          'Content-Type' : 'application/json', 
          'Auth' : `Bearer ${localStorage.getItem('token')}`,
        }
      )
    }

    console.log('GETTASKSTV 1 => ', url, ' => ', options);
    return new Promise((resolve, reject) => {
      this.http.get(url, options).subscribe((data) => {
        let tasks = JSON.parse(JSON.stringify(data));
       console.log('GETTASKSTV 2 => ', tasks);
        resolve(tasks);
        this.tasksTV.next(tasks);
      },err => { 
          return [];
      });
    })    

  }


  //################################## GET DETAILS OF SPECIFIC COMMESSA ##################################
  //################################## GET DETAILS OF SPECIFIC COMMESSA ##################################
  public createTask(data, alarm?) {

    const url = environment.url + environment.code + '/createtask';
    const options = { 
      headers: new HttpHeaders(
        { 
          'Access-Control-Allow-Origin': '*', 
          'Access-Control-Allow-Methods': 'GET', 
          'Accept': '*/*', 
          'Content-Type' : 'application/json', 
          'Auth' : `Bearer ${localStorage.getItem('token')}`,
          'x-api-key' : `GyW8UsIDcS95KbAen9lBd7OVB9A3PaKP8gdcKrGx`,
        }
      )
    }
    console.log('CREATETASK 1 => ', url, ' => ', data);

    return new Promise((resolve, reject) => {
      this.http.post(url, data, options).subscribe((res) => {
        console.log('CREATETASK 2 => ', res, ' => ALARM = ', alarm);

        if (alarm){
          const timestamp = Math.floor(new Date().getTime() / 1000);
          let data2 = {...alarm,...{status:"managed", status_timestamp: timestamp}};
          console.log('POST TASK UPDATE ALARM => ', data2);
          this.vs.postAlarms(data2);
        }
        resolve(res);
      },err => { 
          return [];
      });
    })    

  }
}


 

