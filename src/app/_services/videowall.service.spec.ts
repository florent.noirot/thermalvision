import { TestBed } from '@angular/core/testing';

import { VideowallService } from './videowall.service';

describe('VideowallService', () => {
  let service: VideowallService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideowallService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
