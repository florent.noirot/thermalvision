import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { AlarmsService } from '../../_services/alarms.service';
import { MatDialog } from '@angular/material/dialog';
import { VideowallService } from '../../_services/videowall.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'alarms',
  templateUrl: './alarms.component.html',
  styleUrls: ['./alarms.component.css']
})
export class AlarmsComponent implements OnInit, OnDestroy {

  public type = "Segnalazione - Altro";
  public from;
  public to;

  @Output() result = new EventEmitter();
  public raw;
  public details;
  public search = "";
  public trimmed = "";
  public tenant$: Subscription;
  public tenant = {};
  public alarms = [];
  public alarms$: Subscription;

  // #########################################
  // ############## CONSTRUCTOR ##############
  // #########################################
  constructor(
    public alarmsService: AlarmsService,
    public dialog: MatDialog,
    public videowallService: VideowallService,
  ){
  }


  async ngOnInit() {
    this.alarms$ = this.alarmsService.alarms$.subscribe(data => {
      this.alarms = JSON.parse(JSON.stringify(data));
      // console.log('ALARM SUB => ', data);
    });

    this.tenant$ = await this.videowallService.tenant$.subscribe(async tenant => {
      this.tenant = tenant;
      // console.log('GETALARMS CALL');
      this.alarmsService.getAlarms(this.tenant['city']);
    });
  }

  // ########################################
  // ############## GET FILTER ##############
  // ########################################
  public filterResults(){
    this.trimmed = this.search.trim().toLowerCase();
  }

  // ##########################################
  // ############## OPEN WIZARD ###############
  // ##########################################
  public async openWizard(){
  }


  // ##########################################
  // ############## DESTROYER ##############
  // ##########################################
  ngOnDestroy(){
    this.tenant$.unsubscribe();
    this.alarms$.unsubscribe();
  }
}

