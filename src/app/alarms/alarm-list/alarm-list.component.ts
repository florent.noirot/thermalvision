
import { AfterViewInit, OnInit, OnChanges, Component, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { TableListComponent } from '../../elements/table/table.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Tasks2Component } from '../../tasks/tasks/tasks.component';
import { AlarmsService } from '../../_services/alarms.service';
import { VideowallService } from '../../_services/videowall.service';
import { formatTimestamp } from '../../_services/tools.service';
import * as config from '../../../assets/data/config.js';

@Component({selector: 'alarm-list', templateUrl: './alarm-list.component.html', styleUrls: ['./alarm-list.component.css']})
export class AlarmListComponent extends TableListComponent implements OnChanges, AfterViewInit {

  @Input('data') data;
  @Input('filter') filter;
  @Output() result = new EventEmitter();
  @Output() result2 = new EventEmitter();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public displayedColumns: string[] = config.ALARMS_LIST_COLUMNS;
  public dataSource;
  public emptyTemplate = config.TASK_EMPTY_TEMPLATE;

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(
    public dialog: MatDialog,
    public alarmsService: AlarmsService,
    public vs: VideowallService,
    ){
    super();
  }

  ngOnChanges() {
    this.processData();
  }

  ngAfterViewInit() {
    this.processData();
  }

  // ##########################################
  // ######## TRANSFORM DATA FOR TABLE ########
  // ##########################################
  public processData(){
    this.dataSource = new MatTableDataSource(this.data);
    // console.log('PROCESS DATA => ', this.data);
    this.dataSource.filter = this.filter;
    this.dataSource.paginator = this.paginator;
  }

  // ##########################################
  // ############## OPEN WIZARD ###############
  // ##########################################
  public async openWizard(alarmDetails){
    let data = {impianto: {}};
    // console.log('ALARMDETAILS => ', alarmDetails);
    data['impianto'] = {}
    data['impianto']['wbs'] = alarmDetails['wbs'];
    data['impianto']['rif_impianto'] =  alarmDetails['rif_impianto'];
    data['impianto']['id_impianto'] = alarmDetails['id_impianto'];
    data['impianto']['tenant'] = JSON.parse(localStorage.getItem('tenant'))['city'];
    data['impianto']['utente'] = JSON.parse(localStorage.getItem('profile'))['name'];
    data['impianto']['sede'] = alarmDetails['nome_impianto'];

    data['alarmData'] = alarmDetails;
    // console.log('OPENWIZARD ALARMDETAILS 2 => ', alarmDetails, ' => ', data);
    this.openModal(data, true);
  }


  // ##########################################
  // ############## OPEN MODAL ##############
  // ##########################################
  openModal(data, wizard): void {
    const dialogRef = this.dialog.open(Tasks2Component, {
      width: 'auto',
      maxHeight: '95vh',
      data: data,
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }


  
  // ##########################################
  // ########### SILENCE ALARM MODAL ##########
  // ##########################################
  silenceAlarm(data, index): void {

    const timestamp = Math.floor(new Date().getTime() / 1000);

    let data2 = {...data,...{status:"acknowledged", status_timestamp: timestamp}};
    this.vs.postAlarms(data2);
    this.data[index] = data;
    this.processData();
    setTimeout(() => {
      this.result2.emit();
    }, 2000);  
  }


  formatDate(ts){
    return formatTimestamp(ts, 'DMYHMS');
  }


}
