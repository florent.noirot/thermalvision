import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlarmsManagerComponent } from './alarms-manager/alarms-manager.component';
import { AlarmsComponent } from './alarms/alarms.component';
import { AlarmListComponent } from './alarm-list/alarm-list.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    AlarmsManagerComponent,
    AlarmsComponent,
    AlarmListComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    AlarmsManagerComponent,
    AlarmsComponent,
    AlarmListComponent
  ],
})
export class AlarmsModule { }
