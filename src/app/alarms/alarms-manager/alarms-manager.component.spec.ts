import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlarmsManagerComponent } from './alarms-manager.component';

describe('AlarmsManagerComponent', () => {
  let component: AlarmsManagerComponent;
  let fixture: ComponentFixture<AlarmsManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlarmsManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlarmsManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
