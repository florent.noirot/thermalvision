import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashboardService } from '../../_services/dashboard.service';
import { VideowallService } from '../../_services/videowall.service';
import { Subscription } from 'rxjs';
import { UserService } from '../../_services/user.service';

@Component({selector: 'report-manager', templateUrl: './report-manager.component.html', styleUrls: ['./report-manager.component.css']})
export class ReportManagerComponent implements OnInit, OnDestroy {

  public chartsConfig = [
    {'label': 'alarms', 'type': 'line', 'key': 'alarms', 'aggregation': '', 'title': 'Trend di Allarmi', 'restricted':true, 'class': 'big', 'dimensions': {'height':270, 'width':'100%'}},// WIDGET 1
    {'label': 'alarms2', 'type': 'none2', 'key': 'alarms2', 'aggregation': '', 'title': 'Num Max Allarme per Impianto', 'restricted':true, 'class': 'small', 'dimensions': {'height':300, 'width':'100%'}},
    {'label': 'tasks2', 'type': 'hybrid', 'key': 'tasks2', 'aggregation': '', 'title': 'Andamento Task', 'restricted':true, 'class': 'big', 'dimensions': {'height':270, 'width':'100%'}},
    {'label': 'tasks2', 'type': 'bar_single', 'key': 'tasks2', 'aggregation': '', 'title': 'Tasks di Oggi', 'restricted':true, 'class': 'small', 'dimensions': {'height':270, 'width':'100%'}},
  ];
  public darkMode = 'light';
  public darkMode$: Subscription;
  
  public data = null;
  public data$: Subscription;

  public tenant = null;
  public tenant$: Subscription;

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(
    public dashboardService: DashboardService,
    public videowallService: VideowallService,
    public userService: UserService, 
    ) { }

  async ngOnInit() {

    this.tenant$ = this.videowallService.tenant$.subscribe(async tenant => {
      this.tenant = tenant;
      this.data = null;
      this.getReport(tenant);
    });

    this.data$ = this.dashboardService.data$.subscribe(async data => {
      this.data = data;
      console.log( this.tenant, ' : GET DASHBOARD DATA => ', this.data);
    });

    this.darkMode$ = this.userService.darkMode$.subscribe(async darkMode => {
      this.darkMode = darkMode;
      console.log('DARKMODE CHANGE => ', darkMode);
    });
  }

  public getReport(tenant){
    this.dashboardService.getReport(tenant);
  }

  // ####################################
  // ############ DESTROY ###############
  // ####################################
  ngOnDestroy(): void {
    if(this.darkMode$){this.darkMode$.unsubscribe();}
    if(this.tenant$){this.tenant$.unsubscribe();}
    if(this.data$){this.data$.unsubscribe();}
  }

}


