import { Component, OnInit, OnChanges, Input } from '@angular/core';

@Component({ selector: 'stats', templateUrl: './stats.component.html', styleUrls: ['./stats.component.css'] })
export class StatsComponent implements OnInit, OnChanges {
  @Input("data") data;
  @Input("config") config;
  @Input("darkMode") darkMode;
  
  public stats = {};
  public label = { 
    'num_impianti' : {label: 'Numero Impianti'},
    'task_chiusi' : {label: 'Task Chiusi'},
    'device_raggiungibili' : {label: 'Device Raggiungibili'},
  }

  // ################################### CONSTRUCTOR ###################################
  // ################################### CONSTRUCTOR ###################################
  constructor() { }

  ngOnInit() {}
  ngOnChanges() {
    this.stats = this.data;
    // console.log('STATS => ', this.stats, ' => ', this.config);
  }



}
