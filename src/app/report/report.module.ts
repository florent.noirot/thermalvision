import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { ReportManagerComponent } from './report-manager/report-manager.component';
import { ChartComponent } from './chart/chart.component';
import { StatsComponent } from './stats/stats.component';
import { Stats2Component } from './stats2/stats2.component';



@NgModule({
  declarations: [
    ReportManagerComponent,
    ChartComponent,
    StatsComponent,
    Stats2Component,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    StatsComponent,
    ChartComponent
  ],
})
export class ReportModule { }
