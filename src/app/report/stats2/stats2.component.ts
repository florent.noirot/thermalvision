import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

import * as _moment from 'moment';// import _moment from 'moment';
import { Moment } from 'moment';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';

const moment = _moment;
const MY_MONTHFORMATS = { parse: {dateInput: 'MM/YYYY'}, display: {dateInput: 'MM/YYYY', monthYearLabel: 'MMM YYYY', dateA11yLabel: 'LL', monthYearA11yLabel: 'MMMM YYYY'},};

@Component({
  selector: 'stats2', templateUrl: './stats2.component.html', styleUrls: ['./stats2.component.css'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]}, 
    { provide: MAT_DATE_FORMATS, useValue: MY_MONTHFORMATS }
  ]
})

export class Stats2Component implements OnInit, OnChanges {
  @Input("data") data;
  @Input("config") config;
  @Input("darkMode") darkMode;

  public dateStart = new FormControl(moment().startOf('year'));
  public dateEnd = new FormControl(moment());
  public timeframe = {start: this.dateStart.value._d, end: this.dateEnd.value._d};
  public timeframeLabel = {start: this.dateStart.value._d.toString(), end: this.dateEnd.value._d.toString()};
  public now = moment();

  public stats = {};

  // ################################### CONSTRUCTOR ###################################
  // ################################### CONSTRUCTOR ###################################
  constructor() { }

  ngOnInit() {}
  ngOnChanges() {
    this.stats = this.data;
    console.log('STATS2 => ', this.stats, ' => ', this.config);
  }

   // ################################### HANDLE DATE ###################################
  // ################################### HANDLE DATE ###################################
  handleDate(element, selectedDate: Moment, datepicker: MatDatepicker<Moment>) {
  
    let bData = false, type = 'selected', key;
    const ctrlValue = this[element].value;
    ctrlValue.month(selectedDate.month());
    ctrlValue.year(selectedDate.year());
    this[element].setValue(ctrlValue);
    datepicker.close();
    
    switch(element){
      case 'dateStart': 
        key = 'start';
        bData = true; 
      break;
      case 'dateEnd': 
        key = 'end'; 
        bData = true; 
      break;
    }
    
    this.timeframeLabel[type][key] = ctrlValue._d.toString(); 
    // console.log('A => ', type, ' => ', key, ' => ', ctrlValue._d, ' => ', this.timeframeLabel[type][key]);
    // console.log('HANDLE DATE => ', element, ' => ', this.timeframe);
    // if(bData){this.getData('selected');}
  }


  get dateEndMin() { return this.dateStart.value.toDate(); }
  get dateStartMax() { return this.dateEnd.value.toDate(); }

}
