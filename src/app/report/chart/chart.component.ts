import { Component, OnInit, OnDestroy, AfterViewInit, OnChanges, ViewChild, ElementRef, Input } from '@angular/core';
import { Chart, PieController, BarController, ArcElement, CategoryScale, ChartConfiguration, LineController, LineElement, PointElement, LinearScale, Title, PolarAreaController, RadarController, RadialLinearScale, BarElement, Tooltip, Legend } from 'chart.js';

const chartColors = ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)', 'rgba(235, 139, 44, 1)', 'rgba(34, 142, 215, 1)', 'rgba(245, 196, 76, 1)', 'rgba(173, 122, 235, 1)', 'rgba(245, 149, 54, 1)'];
const chartColorsBar = ['rgba(255, 99, 132, 0.9)', 'rgba(54, 162, 235, 0.9 )', 'rgba(255, 206, 86, 0.9)', 'rgba(75, 192, 192, 0.9)', 'rgba(153, 102, 255, 0.9)', 'rgba(255, 159, 64, 0.9)', 'rgba(235, 139, 44, 0.9)', 'rgba(34, 142, 215, 0.9)', 'rgba(245, 196, 76, 0.9)', 'rgba(173, 122, 235, 0.9)', 'rgba(245, 149, 54, 0.9)'];
const template = {data: {labels: [], datasets: []}, options: null};
const templateDatasets = {
  pie: {backgroundColor: [], data: []}, 
  line : {label: [], data: [], fill: false, borderColor: [], tension: 0.1}, 
  hybrid : {label: [], data: [], backgroundColor: [], borderColor: [], borderWidth: 1}, 
  bar : {label: [], data: [], backgroundColor: [], borderColor: [], maxBarThickness: 20}, 
  bar_single : {label: [], data: [], backgroundColor: [], borderColor: [], borderWidth: 1}, 
  bar2 : {label: [], data: [], backgroundColor: [], borderColor: [], borderWidth: 1},
  radar : {label: [], data: [], backgroundColor: [], borderColor: [], borderWidth: 1}
};
const templateOptions = {
  pie: { title: {display: true, text: 'tasks'} }, 
  line : {scales: {y: {beginAtZero: true, title: { display: true, text: 'allarmi'}}}}, 
  radar : { scale: { ticks: { beginAtZero: true, min: 0, max: 100, stepSize: 20 }, pointLabels: { fontSize: 18 } }, legend: {position: 'left'} }, 
  hybrid : { scales: { x: {stacked: false, title: { display: false, text: 'ore'}}, y: {beginAtZero: true, title: { display: true, text: 'tasks'}} }}, 
  bar : { scales: { x: {stacked: true, title: { display: false, text: 'ore'}}, y: {beginAtZero: true, title: { display: true, text: 'tasks'}} }}, 
  bar_single : { scales: { x: {stacked: true, title: { display: false, text: 'ore'}}, y: {beginAtZero: true, title: { display: true, text: 'tasks'}} }}, 
  bar2 : {responsive: true, scales: {x: {stacked: true, title: { display: false, text: ''}}, y: {stacked: true, title: { display: true, text: 'tasks'}}}}
};

@Component({selector: 'chart', templateUrl: './chart.component.html', styleUrls: ['./chart.component.css']})
export class ChartComponent implements AfterViewInit, OnChanges {
  @Input("data") data;
  @Input("config") config;
  @Input("darkMode") darkMode;
  @Input("periods") periods;
  @ViewChild('chartSelected') elChartSelected: ElementRef;
  public rangeHour = {'from': 0, 'until': 23};
  public chart = {};
  public dataChartOrignal = {};
  public dataChart = {};
  public mode = 'normal';

  // ################################### CONSTRUCTOR ###################################
  // ################################### CONSTRUCTOR ###################################
  constructor(
  ) {
    Chart.register([
      PieController, BarController, ArcElement, CategoryScale, LinearScale, LineController, 
      LineElement, PointElement, BarElement, PolarAreaController, RadarController, RadialLinearScale,
      Tooltip, Legend
    ]);
  }

  ngOnChanges(){
    this.creatingChart(true);
    // console.log('TAKING ONCHANGES : ', this.data);
  }

  ngAfterViewInit() {  
    this.creatingChart(true);
  }

  // ################################### CREATES CHART ###################################
  // ################################### CREATES CHART ###################################
  creatingChart(bProcess){
    let index = 0,
        key = 'set';
    let chartElements = [this.elChartSelected];    
    if(chartElements[0]){
      if(this.chart[key]){
        this.chart[key].destroy();}
      if(this.data !== null){
        if(bProcess){
          this.transformDataForChart(key, this.config, this.data);
        }
        Chart.defaults.color = (this.darkMode == 'light' ? '#000000' : '#ffffff');
        Chart.defaults.font.size = 10;
        this.chart[key] = new Chart(chartElements[index].nativeElement.getContext('2d'), this.dataChart[key]);
      }
      }
    }


  // ################################### TRANSFORM DATA FOR CHAT ###################################
  // ################################### TRANSFORM DATA FOR CHAT ###################################
  transformDataForChart(set, config, raw) {

    if(raw){

      if (config.aggregation != ''){ raw = raw[config.aggregation];}
      // console.log('#####################: ', set, ' => ', config, ' => ', raw);
      let i = 0, 
        tempLabel = [], 
        tempData = {}, 
        type = config.type,
        tempDatasets = [], 
        datasets = JSON.parse(JSON.stringify(templateDatasets[type])), 
        options = JSON.parse(JSON.stringify(templateOptions[type])),
        data = JSON.parse(JSON.stringify(template));
        this.dataChart[set] = {};

      // ################ TRANSFORMING DATA FOR CHART OBJECT ################ 
      // ################ TRANSFORMING DATA FOR CHART OBJECT ################ 
      switch(type){
        
        case 'line':
          //REORDERS OBJECT FOR MM/YYYY
          tempLabel = [];
          tempData = {};
          for(const keyDay in raw){
            tempLabel.push(keyDay);        
            const data2 = raw[keyDay];          
            for(const keyType in data2){
              if (!tempData[keyType]){tempData[keyType] = [data2[keyType]['value']];}
              else {tempData[keyType].push(data2[keyType]['value']);}
            }
          }

          for(const keyType in tempData){
            datasets = JSON.parse(JSON.stringify(templateDatasets[type])), 
            datasets['borderColor'].push(chartColors[i%chartColorsBar.length]);
            datasets['label'] = keyType; 
            datasets['data'] = tempData[keyType];   
            datasets['type'] = type; 
            tempDatasets.push(datasets);
            i++;
          } 

          data['data']['labels'] = tempLabel;
        break;

        case 'bar':       
          for(const key in raw){
            data['data']['labels'].push((key === 'undefined' || key === 'null' ? 'altro' : key));
            datasets['backgroundColor'].push(chartColorsBar[0]);
            datasets['borderColor'].push(chartColors[0]);
            datasets['data'].push(raw[key]['TOTAL']['value']);
            datasets['type'] = type; 
            i++; 
          }
          tempDatasets.push(datasets);
        break;

        case 'bar_single':       
          tempLabel = [];
          tempData = {};
          for(const keyDay in raw){
            if(i<1){
              tempLabel.push(keyDay);        
              const data2 = raw[keyDay];        
              for(const keyType in data2){
                if (!tempData[keyType]){tempData[keyType] = [data2[keyType]['value']];}
                else {tempData[keyType].push(data2[keyType]['value']);}
              }
            }
            else {
              break;
            } 
            i++
          }

          i = 0;
          for(const keyType in tempData){
            let tempType = "bar";
            datasets = JSON.parse(JSON.stringify(templateDatasets[tempType]));
            datasets['backgroundColor'].push(chartColors[i%chartColorsBar.length]);
            datasets['borderColor'].push(chartColors[i%chartColorsBar.length]);
            datasets['type'] = tempType; 
            
            datasets['label'] = keyType; 
            datasets['data'] = tempData[keyType];   
            tempDatasets.push(datasets);
            i++;
          } 

          data['data']['labels'] = tempLabel;
        break;

        //################ HYBRID ################
        //################ HYBRID ################
        case 'hybrid':
          tempLabel = [];
          tempData = {};
          for(const keyDay in raw){
            tempLabel.push(keyDay);        
            const data2 = raw[keyDay];          
            for(const keyType in data2){
              if (!tempData[keyType]){tempData[keyType] = [data2[keyType]['value']];}
              else {tempData[keyType].push(data2[keyType]['value']);}
            }
          }

          for(const keyType in tempData){
            let tempType = "bar";
            switch(keyType){
              case "medio": tempType = "line";break;
            }
            datasets = JSON.parse(JSON.stringify(templateDatasets[tempType]));
            if(tempType != "line"){ datasets['backgroundColor'].push(chartColors[i%chartColorsBar.length]); }
            datasets['borderColor'].push(chartColors[i%chartColorsBar.length]);
            datasets['type'] = tempType; 
            
            datasets['label'] = keyType; 
            datasets['data'] = tempData[keyType];   
            tempDatasets.push(datasets);
            i++;
          } 

          data['data']['labels'] = tempLabel;
        break;    
      }

      // ################ FINALIZING CHART OBJECT ################ 
      // ################ FINALIZING CHART OBJECT ################ 
      
      data['options'] = options;
      
      // switch(type){
      //   default: data['type'] = 'bar'; break;
      //   case "bar2": data['type'] = 'bar'; break;
      //   case "hybrid": data['type'] = 'bar'; break;
      //   case "line": data['type'] = 'line'; break;
      // }


      data['data']['datasets'] = tempDatasets;
      console.log('=====>  ', type ,' CHART : ', data);
    
      this.dataChart[set] = data;
      this.dataChartOrignal[set] = JSON.parse(JSON.stringify(data));
    }
    
  }

}
