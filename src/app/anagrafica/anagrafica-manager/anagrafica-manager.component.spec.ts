import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnagraficaManagerComponent } from './anagrafica-manager.component';

describe('AnagraficaManagerComponent', () => {
  let component: AnagraficaManagerComponent;
  let fixture: ComponentFixture<AnagraficaManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnagraficaManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnagraficaManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
