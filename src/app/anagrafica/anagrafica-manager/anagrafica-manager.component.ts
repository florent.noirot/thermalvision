import { Component, OnInit, OnDestroy } from '@angular/core';
import { AnagraficaService } from '../../_services/anagrafica.service';
import { MatDialog } from '@angular/material/dialog';
import { CommessaDetailsComponent } from '../commessa-details/commessa-details.component';
import { VideowallService } from '../../_services/videowall.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'anagrafica-manager', 
  templateUrl: './anagrafica-manager.component.html', 
  styleUrls: ['./anagrafica-manager.component.css']
})

export class AnagraficaManagerComponent implements OnInit, OnDestroy {

  public data;
  public id;
  public raw;
  public details;
  public search = "";
  public trimmed = "";
  public tenant$: Subscription;
  public tenant = {};

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(
    public anagraficaService: AnagraficaService,
    public dialog: MatDialog,
    public videowallService: VideowallService,
    ) {}

  async ngOnInit() {

    //TRIGGERS NEW LOADING OF COMMESSE WHEN TENANT CHANGES
    this.tenant$ = this.videowallService.tenant$.subscribe(async tenant => {
      this.data = null;
      this.raw = await this.anagraficaService.getCommesse(tenant);
      setTimeout(() => {
        this.data = this.raw;
      }, 1000);
      
      // console.log('AnagraficaManager 2 => ', this.data);
    });
    
  }

  // ##########################################
  // ############## GET FILTER ##############
  // ##########################################
  public filterResults(){
    this.trimmed = this.search.trim().toLowerCase();
  }
  

  // ##########################################
  // ############## GET DETAILS ##############
  // ##########################################
  public async getDetails(id){

    // this.details = await this.anagraficaService.getCommessaDetails(id);
    this.id = id;
    //SHOW MODAL WITH DETAILS
    // console.log('+++++ getDetails 2 => ', id, ' => ', this.id);
    this.openModal();
  }

  
  // ##########################################
  // ############## OPEN MODAL ##############
  // ##########################################
  openModal(): void {

    const dialogRef = this.dialog.open(CommessaDetailsComponent, {
      width: 'auto',
      maxHeight: '95vh',
      data: this.id,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }


  // ##########################################
  // ############## DESTROYER ##############
  // ##########################################
  ngOnDestroy(){
    this.tenant$.unsubscribe();
  }
}
