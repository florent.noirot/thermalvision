import { AfterViewInit, OnInit, OnChanges, Component, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { TableListComponent } from '../../elements/table/table.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import * as config from '../../../assets/data/config.js';

// import { FormBuilder } from '@angular/forms';


@Component({selector: 'commessa-list', templateUrl: './commessa-list.component.html', styleUrls: ['./commessa-list.component.css']})
export class CommessaListComponent extends TableListComponent implements OnChanges, AfterViewInit {

  @Input('data') data;
  @Input('filter') filter;
  @Output() result = new EventEmitter();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public displayedColumns: string[] = config.COMMESSA_LIST_COLUMNS;
  public dataSource;

  // public fields = config.COMMESSA_DETAIL_FIELDS['amministratore'];

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(
    // public formBuilder: FormBuilder
  ){
    super()

    // let form1 = this.formBuilder.group({ firstName: [''], lastName: [''] });
    // console.log('### FORM1 => ', form1);
    // let conf2 = { firstName: [''], lastName: ['']};
    // let form2 = this.formBuilder.group(conf2);
    // console.log('### FORM2 => ', form2);
    // let conf3 = this.generateConfig();
    // let form3 = this.formBuilder.group(conf3);
    // console.log('### FORM3 => ', form3);
  }

  // generateConfig(){
  //   let result = {};

  //   for (let key in this.fields)[
  //     result[this.fields[key][0]] = ['']
  //   ]

  //   console.log('RESULT GENERATE => ', result);
  //   return result;
  // }


  ngOnChanges() {
    this.processData();
  }

  ngAfterViewInit() {
    this.processData();
  }

  // ##########################################
  // ######## TRANSFORM DATA FOR TABLE ########
  // ##########################################
  public processData(){
    this.dataSource = new MatTableDataSource(this.data);
    // console.log('AVI COMMESSALIST => ',  this.filter);
    this.dataSource.filter = this.filter;
    this.dataSource.paginator = this.paginator;
  }

  // ##########################################
  // ######## HANDLES CLICK FOR DETAILS ########
  // ##########################################
  public handleCommessaSelection(id){
    // console.log('handleCommessaSelection => ', id);
    this.result.emit(id);
    
  }
}
