import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommessaDetailsComponent } from './commessa-details.component';

describe('CommessaDetailsComponent', () => {
  let component: CommessaDetailsComponent;
  let fixture: ComponentFixture<CommessaDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommessaDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommessaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
