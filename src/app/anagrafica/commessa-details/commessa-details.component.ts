import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as config from '../../../assets/data/config.js';
import { AnagraficaService } from '../../_services/anagrafica.service';
import { TabComponent } from 'src/app/elements/tab/tab.component';
import { TabListComponent } from 'src/app/elements/tab-list/tab-list.component';

@Component({selector: 'commessa-details', templateUrl: './commessa-details.component.html', styleUrls: ['./commessa-details.component.css']})

export class CommessaDetailsComponent implements OnInit{

  public tabs = config.TABS;
  public configTab = config.TAB_TYPE;
  public allData = null;
  public bChanged = {};
  public bSaved = true;
  public formValid;

  @ViewChild(TabComponent) TabComponent: TabComponent;
  @ViewChild(TabListComponent) TabListComponent: TabListComponent;

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(
    public dialogRef: MatDialogRef<CommessaDetailsComponent>,
    public anagraficaService: AnagraficaService,
    // @Inject(MAT_DIALOG_DATA) public data
    @Inject(MAT_DIALOG_DATA) public id
  ) {
  }

  async ngOnInit(){
    const data = await this.anagraficaService.getCommessaDetails(this.id);
    setTimeout(() => {
      this.allData = JSON.parse(JSON.stringify(data));
    }, 500);
  
  }

  // ##########################################
  // ############## MODAL CLOSE ##############
  // ##########################################
  onNoClick(): void {
    this.dialogRef.close();
  }

  // ##########################################
  // ###### GET CHANGES FROM SPECIFIC TAB #####
  // ##########################################
  getChanges(tab, data): void {
    this.allData[tab] = data.data;
    this.bChanged[tab] = data.bChanged; 
    //this.bSaved = this.changesSaved(this.bChanged);
    let formAnagraficaValid = this.TabComponent.isValid();
    let formTabListValid = this.TabListComponent.isValid();
    this.formValid = false;
    if(formAnagraficaValid && formTabListValid){
      this.formValid = true;
    }
  }

  // ##########################################
  // ###### DETERMINES IF DATA SAVED #####
  // ##########################################
  changesSaved(data) {
    let res = true;
    for(const key in data){
      if (data[key] == true){
          res = false;
      }
    }
    return res;
  }


  // ##########################################
  // ######### DETERMINES TYPE OF TAB #########
  // ##########################################
  getTabType(name) {
    let res = 'standard';
    res = this.configTab[name];

    return res['type'];
  }
    
  public saveAnagrafica = () => {
    if (this.TabComponent.formAnagrafica.valid && this.TabListComponent.formTabList.valid) {
      this.submitForm();
    }
  }

  submitForm() {
    console.log('Tab =>', + this.TabComponent.formAnagrafica.value);
    console.log('Tab list =>', + this.TabListComponent.formTabList.value);
  }
 


}
