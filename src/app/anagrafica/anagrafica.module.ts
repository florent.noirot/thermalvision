import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnagraficaManagerComponent } from './anagrafica-manager/anagrafica-manager.component';
import { CommessaListComponent } from './commessa-list/commessa-list.component';
import { CommessaDetailsComponent } from './commessa-details/commessa-details.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    AnagraficaManagerComponent,
    CommessaListComponent,
    CommessaDetailsComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    AnagraficaManagerComponent,
    CommessaListComponent,
    CommessaDetailsComponent
  ]
})
export class AnagraficaModule { }
