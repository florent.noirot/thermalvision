import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthenticationService } from './_services/authentication.service';
import { UserService } from './_services/user.service';
import { Subscription } from 'rxjs';

@Component({ selector: 'app-root', templateUrl: './app.component.html', styleUrls: ['./app.component.css'] })

export class AppComponent implements OnInit, OnDestroy {
  title = 'residenziale';
  
  public loginStatus$: Subscription;
  public loginStatus = false;
  public personalVisible$: Subscription;
  public personalVisible = true;
  public darkMode$: Subscription;
  private spinner$: Subscription;
  public bSpinner = false;
  public darkMode = null;
  public bLogin = false;
  public messages;

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(public userService: UserService, public authenticationService: AuthenticationService) {}
  ngOnInit(): void {

    console.log('VERSIONE :  280922.1');

    //LOGIN STATUS
    this.loginStatus$ = this.authenticationService.loginStatus$.subscribe(data => {
      // console.log('LOGIN STATUS => ', data);
      this.loginStatus = data;
    });

    // OBSERVABLE MENU VISIBILITY
    this.personalVisible$ = this.userService.personalVisible$.subscribe(data => {
      this.personalVisible = data;
    });

    // OBSERVABLE DARKMODE
    this.darkMode = localStorage.getItem('darkMode');
    this.darkMode$ = this.userService.darkMode$.subscribe(data => {
      this.darkMode = data;
      const storedDarkMode = localStorage.getItem('darkMode');
      if(storedDarkMode){ this.darkMode = storedDarkMode; }
      document.body.dataset.theme = this.darkMode;
    });

    // OBSERVABLE SPINNER
    this.spinner$ = this.userService.loading$.subscribe(data => {
      this.bSpinner = data;
    });
    
  }


  // ##########################################
  // ############## DESTROYER ##############
  // ##########################################
  ngOnDestroy(){
    this.personalVisible$.unsubscribe();
    this.darkMode$.unsubscribe();
    this.loginStatus$.unsubscribe();
    this.spinner$.unsubscribe();
    
  }
}
