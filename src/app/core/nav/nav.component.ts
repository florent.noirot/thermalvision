import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { Subscription } from 'rxjs';
import { CommunicationService } from "../../_services/communication.service";
import { VideowallService } from '../../_services/videowall.service';
import * as websocket from '../../../assets/data/websocket.js';

@Component({ selector: 'navbar', templateUrl: './nav.component.html', styleUrls: ['./nav.component.css'] })
export class NavComponent implements OnInit {

  public menuVisible$: Subscription;
  public menuVisible = true;
  public loginStatus$: Subscription;
  public loginStatus = false;
  public tenant$: Subscription;
  public tenant = {};
  public impianti$: Subscription;
  public impianti = [];
  public profile = null
  public messages = [];
  public unread = {'alerts': 0, tasks: 0};

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(
    public userService: UserService,
    public authenticationService: AuthenticationService, 
    public communicationService: CommunicationService,
    public videowallService: VideowallService,

  ) { 

    this.messages = this.messages.concat(websocket.ALERTS);
    this.messages = this.messages.concat(websocket.TEMPS);
    this.messages = this.messages.concat(websocket.TASKS);

    // console.log('NAV MESSAGES => ', this.messages);

  }


  ngOnInit(): void {

    // OBSERVABLE MENU VISIBILITY
    this.menuVisible$ = this.userService.menuVisible$.subscribe(data => {
      this.menuVisible = data;
    });

    //LOGIN STATUS
    this.loginStatus$ = this.authenticationService.loginStatus$.subscribe(data => {
      this.loginStatus = data;
      this.profile = JSON.parse(localStorage.getItem("profile"));
      // console.log('NAV PROFILE => ', this.profile);
    });

    //LAUNCHING WEBSOCKET CONNECTION
    this.communicationService.messages.subscribe(data => {
      this.messageManager(data);
    });

    //TRIGGERS NEW LOADING OF IMPIANTI WHEN TENANT CHANGES
    // this.tenant$ = this.videowallService.tenant$.subscribe(tenant => {
      // this.videowallService.getImpianti(tenant);
      // console.log('NAV PROFILE => ', this.profile);
    // });

    // this.impianti$ = this.videowallService.impianti$.subscribe(data => {
    //   this.impianti = JSON.parse(JSON.stringify(data));
    //   localStorage.setItem('impianti', JSON.stringify(data));
    // });
  }



  // SEND MESSAGE THROUGH WEBSOCKET
  async sendMsg() {
    // console.log('SND MESSAGE');
    for(let i = 0; i < this.messages.length ;i++){
      const message = this.messages[i];
      // console.log(i, ' => ', this.messages[i]);
      this.communicationService.messages.next(message);
      const delay = Math.floor(1 + Math.random()*(5 - 1 + 1))
      await this.pauseCode(1.5 * 1000);
    }
  }

  pauseCode(ms)  {
    return new Promise( resolve => { setTimeout(resolve, ms); });
  }

  // ##########################################
  // ###### WEBSOCKET MESSAGE MANAGER #########
  // ##########################################
  public messageManager(data){


    switch(data.type){
      case 'alarm':
        console.log('++ MESSAGE ALARM => ', data);
        this.communicationService.setAlerts(data);
        break;
      case 'temperature':
        // console.log('++ MESSAGE TEMP => ', data);
        this.communicationService.setTemps(data);
        break;
      case 'task':
          this.communicationService.setTasks(data);
          break;
      default:
        break;
    }

  }


  // ###############################################
  //  PREVENTS DROPDOWN TO CLOSE WHEN CLICKED INSIDE
  // ###############################################
  public stop(e){
    e.stopPropagation();
  }


  // ##########################################
  // ############## HANDLE PERSONAL ##############
  // ##########################################
  public getUnread(type, unread){
    this.unread[type] = unread;
  }


  // ##########################################
  // ############## HANDLE PERSONAL ##############
  // ##########################################
  public handlePersonalVisibility(){
    this.userService.setPersonalVisibility();
  }


  // ##########################################
  // ############## DESTROYER ##############
  // ##########################################
  ngOnDestroy(){
  
    if(this.menuVisible$){this.menuVisible$.unsubscribe();}
    if(this.loginStatus$){this.loginStatus$.unsubscribe();}
      if(this.tenant$){this.tenant$.unsubscribe();}
    // this.impianti$.unsubscribe();
  }

}