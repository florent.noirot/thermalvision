import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyCheckComponent } from './privacy-check.component';

describe('PrivacyCheckComponent', () => {
  let component: PrivacyCheckComponent;
  let fixture: ComponentFixture<PrivacyCheckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrivacyCheckComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacyCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
