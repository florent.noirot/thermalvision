import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationService } from '../../_services/authentication.service';
import { Router } from '@angular/router';

import { OktaAuthStateService, OKTA_AUTH } from '@okta/okta-angular';
import { AuthState, OktaAuth } from '@okta/okta-auth-js';


@Component({selector: 'privacy-check', templateUrl: './privacy-check.component.html', styleUrls: ['./privacy-check.component.css']})
export class PrivacyCheckComponent implements OnInit {

  // ################################### CONSTRUCTOR ################################### 
  // ################################### CONSTRUCTOR ################################### 
  constructor(
    private router: Router, 
    public dialog: MatDialog, 
    public authenticationService: AuthenticationService, 
    public oktaAuthState: OktaAuthStateService, 
    @Inject(OKTA_AUTH) private oktaAuth: OktaAuth,
  ) {}

  async ngOnInit() {
    await this.handleCallback();
  }


  // ################################### HANDLES CALLBACK URL ################################### 
  // ################################### HANDLES CALLBACK URL ################################### 
  async handleCallback(){
    const that = this;
      console.log('OKTA CALLBACK TOKEN => 1');
      await this.oktaAuth.token.parseFromUrl().then(function(res) {
      let tokens = res.tokens;
      console.log(new Date(), ' : OKTA CALLBACK TOKEN => 2 : ', tokens['accessToken']['accessToken'], ' => ', tokens['refreshToken']['refreshToken']);
      that.oktaAuth.tokenManager.setTokens(tokens);
      let jwtDecode = JSON.parse(atob(tokens['accessToken']['accessToken'].split('.')[1]));
      let temp = {role: jwtDecode['role'], city: jwtDecode['city_id']};
      // let temp = {role: jwtDecode['role'], city: {"role":["admin"],"city":["novara","piemonte","lombardia_ovest"]}};
      // console.log('CALLBACK 1 => SETTOKENS => ',temp);
      localStorage.setItem('role', JSON.stringify(temp));
      localStorage.setItem('token', JSON.stringify(tokens['accessToken']['accessToken']));
    }).catch(function(err) {
      console.log('CALLBACK ERROR => ', err);
      // this.authenticationService.logout();
    });
  }

}
