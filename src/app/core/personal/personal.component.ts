import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { Subscription } from 'rxjs';

@Component({selector: 'personal', templateUrl: './personal.component.html', styleUrls: ['./personal.component.css']})
export class PersonalComponent implements OnInit, OnDestroy {
  
  public darkMode$: Subscription;
  public darkMode = null;

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(public userService: UserService) { }
  ngOnInit(): void {

    // this.darkMode$ = this.userService.darkMode$.subscribe(darkMode => {
    //   this.darkMode = darkMode;
    // });
  }

  // ##########################################
  // ############## HANDLE PERSONAL ##############
  // ##########################################
  public handlePersonalVisibility(){
    this.userService.setPersonalVisibility();
  }

  // ##########################################
  // ########### DARKMODE HANDLING ############
  // ##########################################
  changeTheme(evt) {
    console.log('DARKMODE CHANGE => ', evt.currentTarget.checked, ' => ', evt.currentTarget.checked ? 'dark' : 'light')
    document.body.dataset.theme = evt.currentTarget.checked ? 'dark' : 'light';
    this.userService.setDarkMode(document.body.dataset.theme);
  }


  ngOnDestroy() {
    // if(this.darkMode$){this.darkMode$.unsubscribe();}
  }


}
