import { Component, OnDestroy, OnInit } from '@angular/core';
import { NJWC } from '@engie-group/fluid-design-system/lib-esm/fluid-design-system';
import { UserService } from '../../_services/user.service';
import { Subscription } from 'rxjs';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({ selector: 'menu', templateUrl: './menu.component.html', styleUrls: ['./menu.component.css'] })

export class MenuComponent implements OnInit, OnDestroy {

  activeRoute: string;
  public menuVisible$: Subscription;
  public menuVisible = true;
  public userFunctionalities$: Subscription;
  public userFunctionalities = true;
  public url = 'dashboard';

  // ##########################################
  // ############## CONSTRUCRTOR ##############
  // ##########################################  
  constructor(
    private router: Router, 
    public userService: UserService
    ) {
  }

  ngOnInit(): void {
    
    // NJWC.AutoInit();
    // OBSERVABLE MENU VISIBILITY
    this.menuVisible$ = this.userService.menuVisible$.subscribe(data => {
      this.menuVisible = data;
    });

    //GET USER ROLE
    this.userFunctionalities$ = this.userService.userFunctionalities$.subscribe(data => {
      this.userFunctionalities = data;
      console.log('MENU USERFUNCTIONALITIES => ', data);
    });

  }


  // ##########################################
  // ############## HANDLE PERSONAL ###########
  // ##########################################
  public handleMenuVisibility(){
    this.userService.setMenuVisibility();
  }

  // ##########################################
  // ############## NAVIGATE ###########
  // ##########################################
  navigate(url) {
    this.url = url;
    this.router.navigate([url], {});

  }
  
  ngOnDestroy(){
    this.menuVisible$.unsubscribe();
    this.userFunctionalities$.unsubscribe();
  }

}