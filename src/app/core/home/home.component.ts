import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { UserService } from '../../_services/user.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({ selector: 'home', templateUrl: './home.component.html', styleUrls: ['./home.component.css'] })
export class HomeComponent implements OnInit, OnDestroy {

  public loginStatus$: Subscription;
  public loginStatus = false;

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(
    private router: Router, 
    public userService: UserService,
    public authenticationService: AuthenticationService
  ) { 

    // GETTING LOGINSATUS
    this.loginStatus$ = this.authenticationService.loginStatus$.subscribe(data => {
        this.loginStatus = data;
    });

  }

  ngOnInit(): void {
    this.formEffects();
  }

  ngOnDestroy(): void {
    this.loginStatus$.unsubscribe();
  }

  // ##########################################
  // ############## FAKE LOGIN ###########
  // ##########################################
  fakeLogin(status) {
    this.authenticationService.setLoginStatus(status);
    // setTimeout(() => { this.navigate()}, 2500);
  }

  logout(status) {
    console.log('LOGGING OUT');
    // this.authenticationService.logout();
    this.authenticationService.loginStatus.next(false);
    
  }

  // ##########################################
  // ############## NAVIGATE ###########
  // ##########################################
  navigate() {
    this.router.navigate(['dashboard'], {});
  }

  // ##########################################
  // ############## FORM EFFECTS ##############
  // ##########################################
  formEffects = () => {

    $(window).ready(function() {

      $('input').blur(function() {
        var $this = $(this);
        if ($this.val())
          $this.addClass('used');
        else
          $this.removeClass('used');
      });
    
      var $ripples = $('.ripples');
    
      $ripples.on('click.Ripples', function(e) {
    
        var $this = $(this);
        var $offset = $this.parent().offset();
        var $circle = $this.find('.ripplesCircle');
    
        var x = e.pageX - $offset.left;
        var y = e.pageY - $offset.top;
    
        $circle.css({
          top: y + 'px',
          left: x + 'px'
        });
    
        $this.addClass('is-active');
    
      });
    
      $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
        $(this).removeClass('is-active');
      });
    
    });
  }
} 
