import { CommonModule } from '@angular/common';
import { VideowallModule } from '../videowall/videowall.module';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { MenuComponent } from './menu/menu.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { PersonalComponent } from './personal/personal.component';
import { PrivacyCheckComponent } from './privacy-check/privacy-check.component';

import { WebsocketService } from '../_services/websocket.service';
import { CommunicationService } from '../_services/communication.service';


@NgModule({
  declarations: [
    NavComponent,
    HomeComponent,
    PrivacyComponent,
    MenuComponent,
    PersonalComponent,
    PrivacyCheckComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    VideowallModule
  ],
  exports: [
    NavComponent,
    HomeComponent,
    PrivacyComponent,
    MenuComponent,
    PersonalComponent
  ],
  providers:[
    WebsocketService, 
    CommunicationService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class CoreModule { }
