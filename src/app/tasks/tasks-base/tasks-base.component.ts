import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { TasksService } from '../../_services/tasks.service';
import { TaskDetailsComponent } from '../task-details/task-details.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { VideowallService } from 'src/app/_services/videowall.service';
import { getTimeframe } from '../../_services/tools.service';

@Component({
  selector: 'tasks-base',
  templateUrl: './tasks-base.component.html',
  styleUrls: ['./tasks-base.component.css']
})
export class TasksBaseComponent implements OnInit, OnDestroy {

  public type = "Segnalazione - Altro";
  public timeframe;
  public tenant$: Subscription;
  public tenant = {};

  public tasksTV$: Subscription;
  public tasksTV = [];

  @Output() result = new EventEmitter();
  public data;
  public raw;
  public details;
  public search = "";
  public trimmed = "";
  public emptyTemplate = {
    "impianto": {
    },
    "cliente": {
      "cliente_nome": "Marco",
      "cliente_cognome": "Rossi",
      "cliente_telefono": "123456789",
    },
    "segnalazione": {
      "data_val": "2022-07-06",
      "data_all": "2022-07-07",
      "data_validazione": "2022-04-06T16:06:00",
      "data_allarme": "2022-04-06T16:06:00",
      "note": "Contattare inquilino al piano 2"
    },
    "extra": {
      "tenant": "",
      "nome_utente": "Giada Nassar",
    },
  };

  // #########################################
  // ############## CONSTRUCTOR ##############
  // #########################################
  constructor(
    public tasksService: TasksService,
    public dialog: MatDialog,
    public videowallService: VideowallService,
    ) {
    this.timeframe = getTimeframe();
  }

  async ngOnInit() {
    this.tenant$ = await this.videowallService.tenant$.subscribe(async tenant => {
      this.tenant = tenant;
      this.tasksService.getTasksTV(this.tenant['city']);
    });
    
    this.tasksTV$ = this.tasksService.tasksTV$.subscribe(data => {
      this.data = JSON.parse(JSON.stringify(data));
      console.log('TASKS BASE => ', this.data);
    });
  }

  // ########################################
  // ############## GET FILTER ##############
  // ########################################
  public filterResults() {
    this.trimmed = this.search.trim().toLowerCase();
  }


  // ##########################################
  // ############ GET TASK DETAILS ############
  // ##########################################
  public async getDetails(idTask) {
    let details = this.data[idTask];
    details = this.formatDataFromServer(details);
    // console.log('TASKS BASE GETDETAILS => ', idTask, ' => ', details);
    this.openDetails(details, false);
  }


  // ##########################################
  // ############## OPEN WIZARD ###############
  // ##########################################
  public async openWizard() {

    let temp = new Date();
    let dateWizard = temp.toISOString();

    let newTask = JSON.parse(JSON.stringify(this.emptyTemplate));
    newTask['impianto']['id_impianto'] = '';
    newTask['impianto']['nome_impianto'] = '';
    newTask['extra']['tenant'] = JSON.parse(localStorage.getItem('tenant'))['city'];
    newTask['extra']['nome_utente'] = JSON.parse(localStorage.getItem('profile'))['name'];
    newTask['segnalazione']['data_validazione'] = dateWizard;
    newTask['segnalazione']['data_val'] = dateWizard;
    newTask['segnalazione']['data_allarme'] = dateWizard;
    newTask['segnalazione']['data_all'] = dateWizard;
    // console.log('OPENWIZARD BASE => ', newTask);

    this.openDetails(newTask, true);
  }


  // ##########################################
  // ############## OPEN MODAL ##############
  // ##########################################
  openDetails(data, wizard): void {
    this.result.emit({ data: data, config: { wizard: wizard } });
    //console.log('open => ',data);
  }


  // ##########################################
  // ############## OPEN MODAL ##############
  // ##########################################
  formatData(tasks, impianto) {
    let result = JSON.parse(JSON.stringify(tasks));
    result.forEach(task => {
      task = { ...task, ...{ "id_impianto": impianto['id_impianto'], "sede": impianto['sede'], "indirizzo": impianto['indirizzo'] } }
    });
    // console.log('TASKS FORMAT DATA : ', result);
    return result;
  }

  // ##########################################
  // #### FORMAT DATA FROM SERVER ##########
  // ##########################################
  formatDataFromServer(data) {
    let result = this.emptyTemplate;
    result['impianto']['id_impianto'] = data['sede'];
    result['impianto']['priority'] = 1;
    
    result['cliente']['cliente_nome'] = data['cliente_nome'];
    result['cliente']['cliente_cognome'] = data['cliente_cognome'];
    result['cliente']['cliente_telefono'] = data['cliente_telefono'];
    
    result['segnalazione']['data_val'] = data['open_date'].substring(0,10);
    result['segnalazione']['data_all'] = 'N/A';
    result['segnalazione']['note'] = data['note'];
    result['segnalazione']['task_type'] = data['task_type'];
    console.log('FORMATDATAFROMSERVER 2 => ', data, ' => ', result);    
    return result;
  }


  ngOnDestroy(){
    this.tasksTV$.unsubscribe();
    this.tenant$.unsubscribe();
  }
}
