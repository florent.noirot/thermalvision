import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskDetailsBaseComponent } from './task-details-base.component';

describe('TaskDetailsBaseComponent', () => {
  let component: TaskDetailsBaseComponent;
  let fixture: ComponentFixture<TaskDetailsBaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskDetailsBaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskDetailsBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
