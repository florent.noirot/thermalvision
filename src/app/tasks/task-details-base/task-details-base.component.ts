import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { TasksService } from '../../_services/tasks.service';
import * as config from '../../../assets/data/config.js';

@Component({
  selector: 'task-details-base',
  templateUrl: './task-details-base.component.html',
  styleUrls: ['./task-details-base.component.css']
})
export class TaskDetailsBaseComponent implements OnChanges{


  public tabs = config.TASK_TABS;
  @Input('data') data;
  @Output() result = new EventEmitter();

  public details = null;
  public allData = {};
  public bChanged = {};
  public bSaved = true;
  public bAdding = false;
  public bAdded = false;
  public bComplete = true;
  public selectedTab = 0;
  public configTab = config.TASK_TAB_TYPE;
  public configDetails = {wizard: false};

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(
    public ts: TasksService,
  ) {
  }

  ngOnChanges(){
    console.log('TASKDETAILS BASE => ', this.data);
    this.allData = JSON.parse(JSON.stringify(this.data.data));
    this.configDetails = JSON.parse(JSON.stringify(this.data.config));
    this.bComplete = this.isComplete(this.selectedTab);
  }

  // ##########################################
  // ###### GET CHANGES FROM SPECIFIC TAB #####
  // ##########################################
  getChanges(tab, data): void {
    this.allData[tab] = data.data;
    this.bComplete = this.isComplete(this.selectedTab);
  }

  // ##########################################
  // ###### DETERMINES IF DATA SAVED #####
  // ##########################################
  changesSaved(data) {
    let res = true;
    for(const key in data){
      if (data[key] == true){
          res = false;
      }
    }
    return res;
  }

   
  // ##########################################
  // ###### DETERMINES IF DATA SAVED #####
  // ##########################################
  nextStep(step) {
    this.selectedTab += step;
    this.selectedTab = Math.min(2, this.selectedTab);
    this.bComplete = this.isComplete(this.selectedTab);
  }

  // ##########################################
  // ######### IS TAB DATA COMPLETE ###########
  // ##########################################
  isComplete(indexTab) {
    let res = true;

    const data = this.allData[Object.keys(this.allData)[indexTab]];
    for(const key in data){
      if(data[key] == ''){
        res = false;
      }
    }

    return res;
  }

  // ##########################################
  // ####FORMAT DATA FOR DISPLAYINH ###########
  // ##########################################
  formatData(data) {
    let result = {};

    for(const tab in data){
      for(const key in data[tab]){
        // if(key != 'data_all' && key != 'data_val' && key != 'nome_impianto')
        if(key != 'data_all' && key != 'data_val'){
          result[key] = data[tab][key];
        }
      }
    }
    return result;
  }

  // ##########################################
  // ######### SAVE NEW TASK ###########
  // ##########################################
  async saveNewTask() {
    this.bAdding = true;
    let tempData = this.formatData(this.allData);
    const res = await this.ts.createTask(tempData);
    
    setTimeout(() => {
      console.log('SAVETASK => ', res);
      this.bAdded = true;
      this.bAdding = false;
      this.result.emit(false);

    },1100);

  }


}
