import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksManagerComponent } from './tasks-manager/tasks-manager.component';
import { TaskListComponent } from './task-list/task-list.component';
import { Tasks2Component } from './tasks/tasks.component';
import { SharedModule } from '../shared/shared.module';
import { TasksBaseComponent } from './tasks-base/tasks-base.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { TaskDetailsBaseComponent } from './task-details-base/task-details-base.component';


@NgModule({
  declarations: [
    TasksManagerComponent,
    TaskListComponent,
    Tasks2Component,
    TasksBaseComponent,
    TaskDetailsComponent,
    TaskDetailsBaseComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    TasksManagerComponent,
    TaskListComponent,
    Tasks2Component,
    TasksBaseComponent,
    TaskDetailsComponent,
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class TasksModule { }