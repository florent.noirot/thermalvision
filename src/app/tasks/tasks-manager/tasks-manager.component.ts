import { Component, OnInit } from '@angular/core';
import { TasksService } from '../../_services/tasks.service';

@Component({
  selector: 'tasks-manager',
  templateUrl: './tasks-manager.component.html',
  styleUrls: ['./tasks-manager.component.css']
})
export class TasksManagerComponent implements OnInit {

  public opened = false;
  public taskDetails= null

  constructor(
    public tasksService: TasksService,
  ) { }

  ngOnInit(): void {
  }

  // ##########################################
  // ### DISPLAYS EDITOR ABOVE TASK LIST ######
  // ##########################################
  public openEditor(data){

      this.opened = true;
      this.taskDetails =  data;
  }

  // ##########################################
  // ### TRIGGERS WHEN TASK ADDED ######
  // ##########################################
  public handleCreate(state){

    setTimeout(() => {
      this.opened = state;
      this.taskDetails = {};
      this.tasksService.getTasksTV(JSON.parse(localStorage.getItem('tenant'))['city']);
      console.log('REFRESH TASK LIST');
    }, 1000);


}


}
