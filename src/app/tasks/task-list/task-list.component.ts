import { AfterViewInit, OnInit, OnChanges, Component, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { TableListComponent } from '../../elements/table/table.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import * as config from '../../../assets/data/config.js';
import { formatDateIt } from 'src/app/_services/tools.service';



@Component({selector: 'task-list', templateUrl: './task-list.component.html', styleUrls: ['./task-list.component.css']})
export class TaskListComponent extends TableListComponent implements OnChanges, AfterViewInit {

  @Input('data') data;
  @Input('filter') filter;
  @Output() result = new EventEmitter();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public displayedColumns: string[] = config.TASK_LIST_COLUMNS;
  public dataSource;

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(){
    super();
  }

  ngOnChanges() {
    this.processData();
  }

  ngAfterViewInit() {
    this.processData();
  }

  // ##########################################
  // ######## TRANSFORM DATA FOR TABLE ########
  // ##########################################
  public processData(){
  //  this.dateString();
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.filter = this.filter;
    this.dataSource.paginator = this.paginator;
  }

  // ##########################################
  // ######## HANDLES CLICK FOR DETAILS ########
  // ##########################################
  public handleSelection(id){
    this.result.emit(id);
    
  }

  /*dateString() {
    debugger;
   // let result = JSON.parse(JSON.stringify(tasks));
    this.data.forEach((v, k) => {
      this.data[k]['open_date'] = formatDateIt(v['open_date']);
    });
  }*/
}
