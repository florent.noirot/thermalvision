import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { TasksService } from '../../_services/tasks.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TaskDetailsComponent } from '../task-details/task-details.component';
import { getTimeframe } from '../../_services/tools.service';
import * as config from '../../../assets/data/config.js';
import { Subscription } from 'rxjs';

@Component({
  selector: 'tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})

export class Tasks2Component implements OnInit, OnDestroy {

  public timeframe;
  public data;
  public data3;
  public raw;
  public details;
  public search = "";
  public trimmed = "";
  public emptyTemplate = config.TASK_EMPTY_TEMPLATE;
  public alarmData = null;

  public tasks$: Subscription;

  // #########################################
  // ############## CONSTRUCTOR ##############
  // #########################################
  constructor(
    public tasksService: TasksService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<Tasks2Component>,
    @Inject(MAT_DIALOG_DATA) public data2
  ){
    // debugger;
    this.timeframe = getTimeframe();
    this.data3 = data2.impianto;
    if(data2.alarmData){this.alarmData = data2.alarmData;}

  }

  async ngOnInit() {
    this.tasksService.getTasks('00' + this.data3['wbs'] + '-' +  this.data3['rif_impianto'], this.timeframe['from'], this.timeframe['to']);
    this.tasks$ = this.tasksService.tasks$.subscribe(data => {
      this.raw = JSON.parse(JSON.stringify(data));
      if(this.raw){
        this.data = this.formatData(this.raw, this.data3);
        // console.log('GET TASKS => ', this.data3['wbs'] + '-' +  this.data3['rif_impianto'], ' => ', this.data3, ' => ' , this.data);
      }
    });

  }

  // ########################################
  // ############## GET FILTER ##############
  // ########################################
  public filterResults(){
    this.trimmed = this.search.trim().toLowerCase();
  }
  
  // ##########################################
  // ############ GET TASK DETAILS ############
  // ##########################################
  public async getDetails(idTask){
    let details = this.data[idTask];
    details = this.formatDataFromServer(details);
    this.openModal(details, false);
  }


  // ##########################################
  // ############## OPEN WIZARD ###############
  // ##########################################
  public async openWizard(){

    let temp = new Date();
    let dateWizard = temp.toISOString();

    let newTask = JSON.parse(JSON.stringify(this.emptyTemplate));
    newTask['impianto']['id_impianto'] = '00' + this.data3['wbs'] + '-' + this.data3['rif_impianto'];
    newTask['impianto']['nome_impianto'] = this.data3['sede'];
    newTask['extra']['tenant'] = JSON.parse(localStorage.getItem('tenant'))['city'];
    newTask['extra']['nome_utente'] = JSON.parse(localStorage.getItem('profile'))['name'];
    newTask['segnalazione']['data_validazione'] = dateWizard;
    newTask['segnalazione']['data_val'] = dateWizard;
    newTask['segnalazione']['data_allarme'] = dateWizard;
    newTask['segnalazione']['data_all'] = dateWizard;
    console.log('OPENWIZARD NEW TASK => ', newTask, ' => ', this.data3);
    this.openModal(newTask, true);
  }


  // ##########################################
  // ############## OPEN MODAL ##############
  // ##########################################
  openModal(data, wizard): void {

    let modalData = {data: data, config: {wizard: wizard}};
    if(this.alarmData){ modalData = {...modalData, ...{alarmData: this.alarmData}};}

    console.log('TASKS OPENMODALE => ', data);
    const dialogRef = this.dialog.open(TaskDetailsComponent, {
      width: 'auto',
      maxHeight: '95vh',
      data: modalData,
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  // ##########################################
  // #### FORMAT DATA FOR DISPLAYING ##########
  // ##########################################
  formatData(tasks, impianto) {
    let result = [];

    tasks.forEach(task => {
      task = {...task,...{"id_impianto": impianto['id_impianto'], "sede": impianto['wbs'], "indirizzo": impianto['indirizzo']}}
      result.push(task);
    });
    return result;
  }

  // ##########################################
  // #### FORMAT DATA FROM SERVER ##########
  // ##########################################
  formatDataFromServer(data) {
    let result = this.emptyTemplate;
    result['impianto']['id_impianto'] = data['sede'];
    result['impianto']['priority'] = 1;
    
    result['cliente']['cliente_nome'] = data['cliente_nome'];
    result['cliente']['cliente_cognome'] = data['cliente_cognome'];
    result['cliente']['cliente_telefono'] = data['cliente_telefono'];
    
    result['segnalazione']['data_val'] = data['open_date'].substring(0,10);
    result['segnalazione']['data_all'] = 'N/A';
    result['segnalazione']['note'] = data['note'];
    result['segnalazione']['task_type'] = data['task_type'];
    console.log('FORMATDATAFROMSERVER 2 => ', data, ' => ', result);    
    return result;
  }

  ngOnDestroy(){
    this.tasks$.unsubscribe();
  }

}


