import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TasksService } from '../../_services/tasks.service';
import { getTimeframe } from '../../_services/tools.service';
import * as config from '../../../assets/data/config.js';

@Component({
  selector: 'task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent {


  public tabs = config.TASK_TABS;
  public details = null;
  public allData = {};
  public alarmData = null;
  public bChanged = {};
  public bSaved = true;
  public bAdding = false;
  public bAdded = false;
  public bComplete = true;
  public selectedTab = 0;
  public configTab = config.TASK_TAB_TYPE;
  public configDetails = {wizard: false};

  // ##########################################
  // ############## CONSTRUCTOR ##############
  // ##########################################
  constructor(
    public dialogRef: MatDialogRef<TaskDetailsComponent>,
    public ts: TasksService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.allData = JSON.parse(JSON.stringify(data.data));
    if(data.alarmData){this.alarmData = JSON.parse(JSON.stringify(data.alarmData));}
    this.configDetails = JSON.parse(JSON.stringify(data.config));
    console.log('###### CONST TASKDETAILS 2 => ', data);
  }

  // ##########################################
  // ############## MODAL CLOSE ##############
  // ##########################################
  onNoClick(): void {
    this.dialogRef.close();
  }

  // ##########################################
  // ###### GET CHANGES FROM SPECIFIC TAB #####
  // ##########################################
  getChanges(tab, data): void {
    this.allData[tab] = data.data;
    // this.bChanged[tab] = data.bChanged; 
    // this.bSaved = this.changesSaved(this.bChanged);
    this.bComplete = this.isComplete(this.selectedTab);
    // console.log("=====> TASKDETAILS CHANGED => ", data.data)
  }

  // ##########################################
  // ###### DETERMINES IF DATA SAVED #####
  // ##########################################
  changesSaved(data) {
    let res = true;
    for(const key in data){
      if (data[key] == true){
          res = false;
      }
    }
    return res;
  }

   
  // ##########################################
  // ###### DETERMINES IF DATA SAVED #####
  // ##########################################
  nextStep(step) {
    this.selectedTab += step;
    this.selectedTab = Math.min(2, this.selectedTab);
    this.bComplete = this.isComplete(this.selectedTab);
  }

  // ##########################################
  // ######### IS TAB DATA COMPLETE ###########
  // ##########################################
  isComplete(indexTab) {
    let res = true;
    const data = this.allData[Object.keys(this.allData)[indexTab]];
    // console.log('ISCOMPLETE => ', indexTab, ' => ', data);
    for(const key in data){
      if(data[key] == ''){
        res = false;
      }
    }

    return res;
  }

  // ##########################################
  // ####FORMAT DATA FOR DISPLAYINH ###########
  // ##########################################
  formatData(data) {
    let result = {};

    for(const tab in data){
      for(const key in data[tab]){
        // if(key != 'data_all' && key != 'data_val' && key != 'nome_impianto'){
        if(key != 'data_all' && key != 'data_val'){
          const val = (key == 'priority' ? parseInt(data[tab][key], 10) : data[tab][key]);
          result[key] = val;
        }
      }
    }
    return result;
  }

  // ##########################################
  // ######### SAVE NEW TASK ###########
  // ##########################################
  async saveNewTask() {
    this.bAdding = true;
    let tempData = this.formatData(this.allData);
    let res;
    if(this.alarmData){
      res = await this.ts.createTask(tempData, this.alarmData);
    } else { res = await this.ts.createTask(tempData); }
    
    setTimeout(() => {
      console.log('SAVETASK => ', res);
      this.bAdded = true;
      this.bAdding = false;
      const timeframe = getTimeframe();
      this.ts.getTasks(tempData['id_impianto'], timeframe['from'], timeframe['to']);

    },1500);

  }
}



// {    

//   "tenant": "lombardia",
//   "id_impianto": "0047075-001",
//   "priority": 1,
//   "task_type": "Segnalazione - Altro",
//   "cliente_nome": "Giuseppe",
//   "cliente_cognome": "Rossi",
//   "cliente_telefono": "12345678",
//   "data_validazione": "2022-04-06T16:06:00",
//   "data_allarme": "2022-04-06T16:06:00",
//   "nome_utente": "Giada Nassar",
//   "note": "Contattare inquilino al piano 2"



// cliente_cognome:  "bbb"
// cliente_nome: "aaa"
// cliente_telefono: "ccc"
// data_allarme: "2022-09-20T08:29:57.834Z"
// data_validazione: "2022-09-20T08:29:57.834Z"
// id_impianto: "43044-001"
// nome_utente: "Florent Noirot"
// note: "test3"
// priority: 1
// task_type: "Segnalazione - Altro"
// tenant: "adriatica"