import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { VideowallService } from '../../_services/videowall.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public search = "";
  public trimmed = "";
  public filteredImpianti$: Subscription;
  public filteredImpianti = {'features': []};
  
  constructor(public userService: UserService, public videowallService: VideowallService) { }

  ngOnInit(): void {

    this.filteredImpianti$ = this.videowallService.filteredImpianti$.subscribe(data => {
      this.filteredImpianti = data;
      // console.log('SEARCH filteredImpianti => ', this.filteredImpianti);
    });
  
  }
  
  // ##########################################
  // ############# FILTER INPIANTI ############
  // ##########################################
  public filterResults(){
    this.trimmed = this.search.trim().toLowerCase();
    this.videowallService.filterImpianti(this.trimmed); 
  }

  // ##########################################
  // ############# CENTERS MAP ON IMPIANTO ############
  // ##########################################
  public selectImpianti(i){
    this.trimmed = this.search.trim().toLowerCase();
    this.videowallService.centerPoint.next({coordinates: this.filteredImpianti['features'][i]['geometry']['coordinates'], zoom: 14}); 
  }


  ngOnDestroy(): void {
    this.filteredImpianti$.unsubscribe();
  }
}
