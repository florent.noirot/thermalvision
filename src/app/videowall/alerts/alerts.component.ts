import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { CommunicationService } from '../../_services/communication.service';
import { Subscription } from 'rxjs';
import { VideowallService } from '../../_services/videowall.service';

@Component({
  selector: 'alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit, OnDestroy {

  public alerts$: Subscription;
  public alerts = {};
  public tempAlerts = [];
  public unread = 0;
  public tenant$: Subscription;
  public tenant = {};

  @Input("type") type;
  @Output() result = new EventEmitter();

  constructor(
    public cs: CommunicationService,
    public videowallService: VideowallService,
  ) {
  }

  ngOnInit(): void {
      this.alerts[this.type] = [];
      
      this.tenant$ = this.videowallService.tenant$.subscribe(async tenant => {
        this.tenant =  tenant;
      });
      
      // SUBSCRIBING TO ALERTS OBSERVABLE FOR NEW ELEMENT RECEIVED THROUGH WEBSOCKET
      this.alerts$ = this.cs.alerts$.subscribe(res => {
        this.tempAlerts = JSON.parse(JSON.stringify(this.alerts[this.type].reverse()));
        if (res){
          let data = JSON.parse(JSON.stringify(res));
          if (this.type == 'videowall'){
            data['read'] = true;
          } else {
            data['read'] = false;
          }

          if (data['tenant'] == this.tenant['city']){
            this.tempAlerts.push(data);
            this.alerts[this.type] = JSON.parse(JSON.stringify(this.tempAlerts));
            this.alerts[this.type].reverse();
            this.unread++;
            this.emitNotification();
          }

        }
      });
  }


  //CHANGES OBJECT TO FLAG CLICKED ELEMENT AS READ
  public readAlert(index){
    console.log(this.type, ' : ', index, ' => ', this.alerts[this.type]);
    let tempAlerts =  JSON.parse(JSON.stringify(this.alerts[this.type]));
    // const id = tempAlerts.length - 1 - index
    const id = index
    tempAlerts[id]['read'] = true;
    this.alerts[this.type] = JSON.parse(JSON.stringify(tempAlerts));
    this.unread--;
    this.unread = Math.max(0, this.unread);
    this.emitNotification();
  }

  //SENDS TO PARENTS NUMBER OF UNREAD
  public emitNotification(){
    this.result.emit(this.unread);
  }



  ngOnDestroy(){
    this.alerts$.unsubscribe();
    this.tenant$.unsubscribe();
  }
}
