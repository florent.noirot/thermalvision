import { Component, OnChanges, ViewChild, ElementRef, Input, ChangeDetectionStrategy } from '@angular/core';
import { Chart, CategoryScale, ChartConfiguration, PointElement, LineController, LineElement, LinearScale } from 'chart.js';
import { UserService } from '../../_services/user.service';
import { Tasks2Component } from '../../tasks/tasks/tasks.component';
import { MatDialog } from '@angular/material/dialog';

@Component({ selector: 'bottom-element', templateUrl: './bottom-element.component.html', styleUrls: ['./bottom-element.component.css'],
changeDetection: ChangeDetectionStrategy.OnPush})
export class BottomElementComponent implements OnChanges {
  
  @Input("data") data;
  @Input("timestamp") timestamp;
  public ana;

  // ################################### CONSTRUCTOR ###################################
  // ################################### CONSTRUCTOR ###################################
  constructor(
    public userService: UserService,
    public dialog: MatDialog
  ) {
    Chart.register([CategoryScale, LinearScale, LineController,  LineElement, PointElement]);
  }

  ngOnChanges(): void {
    // if(this.type == 'sensore_multispace'){this.creatingChart();}
    this.ana = this.data.ana;
  }


// ##########################################
// ############## OPEN MODAL ##############
// ##########################################
openModal(): void {
  console.log('BOTTOM OPENMODAL : ', this.data.ana);
  const dialogRef = this.dialog.open(Tasks2Component, {
    width: 'auto',
    maxHeight: '95vh',
    data: {impianto: this.data.ana},
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    // this.animal = result;
  });
}

  // ################################### DESTRUCTOR ###################################
  // ################################### DESTRUCTOR ###################################
  ngOnDestroy() {
  }

}
