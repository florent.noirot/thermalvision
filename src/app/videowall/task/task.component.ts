import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { generateRandom, formatTimestamp } from '../../_services/tools.service';

@Component({
  selector: 'task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnChanges {

  @Input('index') index;
  @Input('data') data;
  @Output() result = new EventEmitter();
  public id;

  // ##########################################
  // ############## CONSTRUCTOR ###############
  // ##########################################
  constructor() { 
  }

  ngOnChanges(): void {
    if(this.data){
      this.id = this.data['id']+ "_" + generateRandom(6);
      // console.log('SHOWING ALERT => ', this.id);
    }
  }

  // ##########################################
  // ######## HANDLES CLICK OF TASK ########
  // ##########################################
  public readTask(id){
    // console.log('readAlert => ', raw, ' => ', id);
    if (this.data['read'] == false){
      this.result.emit(id);
    }
    
  }


  formatDate(ts){
    return formatTimestamp(ts, '');
  }


}
