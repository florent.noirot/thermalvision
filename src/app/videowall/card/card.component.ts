import { Component, OnInit, OnDestroy, AfterViewInit, OnChanges, ViewChild, EventEmitter, Output, Input } from '@angular/core';

@Component({ selector: 'card', templateUrl: './card.component.html', styleUrls: ['./card.component.css']})
export class CardComponent implements OnInit{

  public data;
  public button = false;
  public dataDynamic;
  public timestamp;
  @Input("data") dataInput;
  @Output() result = new EventEmitter();

  // ################################### CONSTRUCTOR ###################################
  // ################################### CONSTRUCTOR ###################################
  constructor() {}
  ngOnInit() { 
    this.data = this.dataInput || this.dataDynamic;
    this.button = (this.dataInput ? true : false);
  }

  // ################################### REMOVE ELEMENT FROM PANEL ###################################
  // ################################### REMOVE ELEMENT FROM PANEL ###################################
  // removeElement(data) {
  //   this.result.emit(data);
  // }
  
}