import { Component, OnInit } from '@angular/core';
import * as config from '../../../assets/data/config.js';
import { UserService } from '../../_services/user.service';
import { VideowallService } from '../../_services/videowall.service';
import { Subscription } from 'rxjs';

@Component({selector: 'city', templateUrl: './city.component.html', styleUrls: ['./city.component.css']})
export class CityComponent implements OnInit {

  public tenants = config.TENANTS;
  public selectedTenant = "";
  public selectedTenantIndex = 0;
  public selectedTenant$: Subscription;


  constructor(
    public videowallService: VideowallService,
    public userService: UserService
    ) {
    
    const userTenants = JSON.parse(localStorage.getItem('role'))['city'];
    this.tenants = this.tenants.filter(el => {
      if(el['city'] == ''){return true;}
      else return userTenants.includes(el.city);
    })
    // console.log('USER TENANTS => ', this.tenants);

    // SELECTED TENANT
    this.selectedTenant$ = this.videowallService.tenant$.subscribe(data => {
      this.selectedTenant = data;
      this.getTenantIndex();
    });
  }

  ngOnInit(): void {
  }

  getTenantIndex(){
    this.tenants.forEach((el, index) => {
        // console.log('+++ 1 : ', this.selectedTenant['label'], ' => ', el['label'] );
        if (this.selectedTenant['label'] == el['label']){
        // console.log('GETTENANTINDEX 2 : ', this.selectedTenantIndex, ' => ', this.selectedTenant['label'] );
        this.selectedTenantIndex = index;
      }
    })
  }

  // TENANT CHANGE 
  onChange(id) {
    console.log('<<<< TENANT CHANGE : ', this.tenants[id]);
    this.videowallService.setTenant(this.tenants[id]);
  }
}
