import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../../_services/communication.service';
import { Subscription } from 'rxjs';
import { VideowallService } from '../../_services/videowall.service';

@Component({selector: 'ticker', templateUrl: './ticker.component.html', styleUrls: ['./ticker.component.css'] })
export class TickerComponent implements OnInit {
  public temps$: Subscription;
  public temps = [];
  public tempTemps = [];
  public tenant$: Subscription;
  public tenant = {};

  // ################################### CONSTRUCTOR ###################################
  // ################################### CONSTRUCTOR ###################################
  constructor(
    public cs: CommunicationService,
    public videowallService: VideowallService,
  ) {
  }


  ngOnInit(): void {
    this.tenant$ = this.videowallService.tenant$.subscribe(async tenant => {
      this.tenant =  tenant;
    });
    
    // SUBSCRIBING TO ALERTS OBSERVABLE FOR NEW ELEMENT RECEIVED THROUGH WEBSOCKET
    this.temps$ = this.cs.temps$.subscribe(data => {
      // console.log('TICKER 0 TEMPS => ', data);
      if (data){
        if(data['tenant'] == this.tenant['city']){
          if(data['source'] == "coster" && this.isMandata(data['value'])){
            this.manageTemps(data);
          }
          if(data['temperature_name']){
            if(data['temperature_name'].toLowerCase().includes('mand') && parseFloat(data['value']) > 0){
              this.manageTemps(data);
            }
          }
        }
      }
    });
  }

  // CHECKS IF TEMPERATURE MANDATA EXISTS
  public isMandata(data){
    for (const key in data){
      if (data[key]['alias'] == 'Mandata'){
        return true;
      }
    }
    return false;
  }

  // CHECKS IF TEMPERATURE MANDATA EXISTS
  public manageTemps(data){
    this.addLowest10(data);
    this.temps = JSON.parse(JSON.stringify(this.tempTemps));
    // this.temps.reverse();
    if(this.temps.length > 16){this.temps.pop();}
    console.log('=====> TICKER 3 TEMPS => ', this.temps);
  }

  // CHECKS IF TEMPERATURE MANDATA EXISTS
  public addLowest10(temp){
    let temp2 = JSON.parse(JSON.stringify(temp));
    let minTemp = 1000.0;
    
    if(temp.source == "coster"){
      for(const key in temp.value){
        if (parseFloat(temp.value[key]['value']) < minTemp){
          minTemp = parseFloat(temp.value[key]['value']);
        }
      }
    }
    else {
      minTemp =  parseFloat(temp.value);
    }
    temp2['composite'] = minTemp
    console.log('---> ', temp2, ' => ##### NEW TEMP MIN => ', temp2['nome_impianto'], ' => ', temp2['composite']);

    this.tempTemps.push(temp2);
    // console.log('TEMPS TICKER BEFORE SORT => ', this.tempTemps);
    this.tempTemps = this.tempTemps.sort((a,b) => a['composite'] - b['composite']); 

    // REMOVES DUPLICATES WITH SETS
    // this.tempTemps = Array.from(new Set(this.tempTemps.map(a => a)));
                          

  }



  // TO CREATE PAGES OF $ TEMPERATURES
  public counter(data){
    let length = Math.ceil(data.length / 3)
    // console.log('DATA LENGTH => ', length);
    return new Array(length)
  }


  // ################################### DESTROY ###################################
  // ################################### DESTROY ###################################
  ngOnDestroy(){
    this.temps$.unsubscribe();
    this.tenant$.unsubscribe();
  }

}
