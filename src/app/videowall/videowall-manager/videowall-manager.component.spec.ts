import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideowallManagerComponent } from './videowall-manager.component';

describe('VideowallManagerComponent', () => {
  let component: VideowallManagerComponent;
  let fixture: ComponentFixture<VideowallManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideowallManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideowallManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
