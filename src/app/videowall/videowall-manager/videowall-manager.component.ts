import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { VideowallService } from '../../_services/videowall.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'videowall-manager',
  templateUrl: './videowall-manager.component.html',
  styleUrls: ['./videowall-manager.component.css']
})
export class VideowallManagerComponent implements OnDestroy {

  public tenant$: Subscription;
  public tenant = null;
  public search = "";
  public trimmed = "";

  public loading$: Subscription;
  public loading = false;

  // ################################### CONSTRUCTOR ################################### 
  // ################################### CONSTRUCTOR ################################### 
  constructor(public userService: UserService, public videowallService: VideowallService) {
    this.loading$ = this.videowallService.loading$.subscribe(loading => {

      // console.log('VIDEOWALL LOADING => ', loading);
      this.loading = loading;
    });
  }

  async ngOnDestroy() {
    this.loading$.unsubscribe();
  }

  // ##########################################
  // ############# FILTER INPIANTI ############
  // ##########################################
  public filterResults(){
    this.trimmed = this.search.trim().toLowerCase();
    this.videowallService.filterImpianti(this.trimmed); 
  }


}
