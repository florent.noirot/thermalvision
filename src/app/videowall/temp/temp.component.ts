import { Component, OnChanges, Input } from '@angular/core';
import { generateRandom, formatTimestamp } from '../../_services/tools.service';

@Component({
  selector: 'temp',
  templateUrl: './temp.component.html',
  styleUrls: ['./temp.component.css']
})
export class TempComponent implements OnChanges {

  @Input('data') data;
  @Input('type') type;
  public id;

  constructor() { 
  }

  ngOnChanges() {
    if(this.data){
      this.id = this.data['id']+ "_" + this.type + "_" + generateRandom(6);
      // console.log('SHOWING TEMP => ', this.id, ' => ', this.data);
      if (typeof this.data.value == 'string'){
        // let temps = {};
        // for(let i = 0; i<2; i++){
        //   let var1 = 'temp_' + i;
        //   temps[var1] = this.data.value;
        // }
        // this.data.value = temps;
        this.data.value = {'temp' : {value: this.data.value}};
      }
    }
  }

  formatDate(ts){
    return formatTimestamp(ts, '');
  }

}
