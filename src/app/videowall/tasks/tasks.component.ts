import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { CommunicationService } from '../../_services/communication.service';
import { Subscription } from 'rxjs';
import { VideowallService } from '../../_services/videowall.service';

@Component({
  selector: 'tasks2',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit, OnDestroy {

  public tasks$: Subscription;
  public tasks = [];
  public tempTasks = [];
  public unread = 0;
  public tenant$: Subscription;
  public tenant = {};

  @Input("type") type;
  @Output() result = new EventEmitter();

  constructor(
    public cs: CommunicationService,
    public videowallService: VideowallService,
  ) {
  }


  ngOnInit(): void {

    this.tenant$ = this.videowallService.tenant$.subscribe(async tenant => {
      this.tenant =  tenant;
    });
    
    this.tasks[this.type] = [];
    // SUBSCRIBING TO ALERTS OBSERVABLE FOR NEW ELEMENT RECEIVED THROUGH WEBSOCKET
    this.tasks$ = this.cs.tasks$.subscribe(res => {
      this.tempTasks = JSON.parse(JSON.stringify(this.tasks[this.type].reverse()));

      if (res){
        let data = JSON.parse(JSON.stringify(res));
        if (this.type == 'videowall'){
          data['read'] = true;
        } else {
          data['read'] = false;
        }

        if (data['tenant'] == this.tenant['city']){
          this.tempTasks.push(data);
          this.tasks[this.type] = JSON.parse(JSON.stringify(this.tempTasks));
          this.tasks[this.type].reverse();
          this.unread++;
          this.emitNotification();
        }
      }
    });
} 


  //CHANGES OBJECT TO FLAG CLICKED ELEMENT AS READ
  public readTask(index){
    console.log(this.type, ' : ', index, ' => ', this.tasks[this.type]);
    let tempAlerts =  JSON.parse(JSON.stringify(this.tasks[this.type]));
    // const id = tempAlerts.length - 1 - index
    const id = index
    tempAlerts[id]['read'] = true;
    this.tasks[this.type] = JSON.parse(JSON.stringify(tempAlerts));
    this.unread--;
    this.unread = Math.max(0, this.unread);
    this.emitNotification();
  }

  //SENDS TO PARENTS NUMBER OF UNREAD
  public emitNotification(){
    this.result.emit(this.unread);
  }

  ngOnDestroy(){
    this.tasks$.unsubscribe();
    this.tenant$.unsubscribe();
  }
}
