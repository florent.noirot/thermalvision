import { Component, OnInit, OnDestroy, ComponentFactoryResolver, Injector, EventEmitter, Output, ComponentRef } from '@angular/core';
import { environment } from '../../../environments/environment';
import { VideowallService } from '../../_services/videowall.service';
import { UserService } from '../../_services/user.service';
import { Subscription } from 'rxjs';
import { SVGInjector } from '@tanem/svg-injector'
import { CardComponent } from '../card/card.component';
import * as mapboxgl from 'mapbox-gl';
import * as _ from 'lodash';

const styleMap = {light: 'mapbox://styles/mapbox/light-v10', dark: 'mapbox://styles/mapbox/dark-v10'};
const deltaY = 0.08;
const deltaX = 0.30;

@Component({ selector: 'maps', templateUrl: './maps.component.html', styleUrls: ['./maps.component.css']})
export class MapsComponent implements OnInit, OnDestroy {
  
    @Output() result = new EventEmitter();
    public map;
    public compRef: ComponentRef<CardComponent>;
    public realtimeTime;

    public tenant$: Subscription;
    public tenant = null;

    public impianti$: Subscription;
    public impianti = null;
    public filteredImpianti$: Subscription;
    public filteredImpianti = null;

    public darkMode$: Subscription;
    public darkMode = null;

    public centerPoint$: Subscription;
    public centerPoint = null;

    public mapPopups = {};
    public mapMarkers = [];

    // ################################### CONSTRUCTOR ################################### 
    // ################################### CONSTRUCTOR ################################### 
    constructor(
      public userService: UserService, 
      public videowallService: VideowallService,
      private injector: Injector, 
      private resolver: ComponentFactoryResolver
    ) {}

    async ngOnInit() {
      setTimeout(() => {
        
        this.tenant$ = this.videowallService.tenant$.subscribe(async tenant => {
          this.tenant = tenant;
          await this.videowallService.getImpianti(tenant);
          this.createMap(this.darkMode, tenant);    
          this.removeAllMarkers();
          this.realtimeTime = new Date();
          this.createPinLayer(this.impianti);
        });
  
        this.filteredImpianti$ = this.videowallService.filteredImpianti$.subscribe(data => {
          // console.log('--> MAPS FILTEREDIMPIANTI SUBSCRIPTION : ', data.features.length);
          this.impianti = JSON.parse(JSON.stringify(data));
          this.removeAllMarkers();
          this.realtimeTime = new Date();
          this.createPinLayer(this.impianti);
        });

        this.darkMode$ = this.userService.darkMode$.subscribe(darkMode => {
          this.darkMode = darkMode;
          this.createMap(darkMode, this.tenant);    
          this.removeAllMarkers();
          this.realtimeTime = new Date();
          this.createPinLayer(this.impianti);
        });

        this.centerPoint$ = this.videowallService.centerPoint$.subscribe(center => {
          this.centerPoint = center;
          this.centerMap(this.centerPoint);
        });

      }, 100);
    }



  // ################################### CREATES MAP ################################### 
  // ################################### CREATES MAP ################################### 
  public async createMap(darkMode, center){

    // console.log('CREATEMAPS => ', center);
    let data = {'center': null};
    data['center'] = new mapboxgl.LngLat(center['coordinates'][1], center['coordinates'][0]);
    data['bounds'] = new mapboxgl.LngLatBounds(
      [center['coordinates'][1]- deltaX, center['coordinates'][0] - deltaY], 
      [center['coordinates'][1] + deltaX, center['coordinates'][0] + deltaY]
    );   
    this.map = null;

    this.map = new mapboxgl.Map({
          style: styleMap[darkMode], 
          container: 'mapContainer', 
          accessToken: environment.mapbox.accessToken,
          attributionControl: false, 
          dragRotate: false, 
          touchZoomRotate: false, 
          pitchWithRotate: false,
          refreshExpiredTiles: false, 
          center: data.center, 
          zoom: center['zoom'], 
          minZoom:4, maxZoom:16, 
          // maxBounds: data.bounds
        });   
        this.map.addControl(new mapboxgl.NavigationControl(), 'bottom-right');
      return null;
  }


  // ################################### CREATES MAP ################################### 
  // ################################### CREATES MAP ################################### 
  public centerMap(center){

    const latlon = new mapboxgl.LngLat(center['coordinates'][1], center['coordinates'][0]);
    this.map.setCenter(latlon);
    this.map.setZoom(15);
  }


  // ################################### CREATES PIN LAYER ################################### 
  // ################################### CREATES PIN LAYER ################################### 
  public createPinLayer(pins){
    const name = 'impianto';
    
    // console.log('##### CREATING PIN LAYER #### => ', pins.features.length);   
    const that = this;
    
    //########################## CREATING ICONS #################################
    //########################## CREATING ICONS #################################
    pins.features.forEach(function (marker, index) {  
                
      //CREATE DIV
      let el = document.createElement('div'); 
      el.className = 'marker ' + 'div_' + index + '_' + marker.properties['wbs'] + '_' + marker.properties['id'];
      
      //CREATE SVG INSIDE DIV
      let markerClass = 'marker_' + index + '_' + marker.properties['wbs'] + '_' + marker.properties['id'];          
      // console.log('--> ', markerClass);
      let svg = document.createElement('div'); 
      svg.className =  name + ' '  + markerClass;
      svg.dataset.src = marker.properties.squadre.colore == "#ff0000" ? "assets/images/icons/bell.svg" : "assets/images/icons/pin.svg";
      el.appendChild(svg); 


      //########################## MOUSE CLICK #################################
      //########################## MOUSE CLICK #################################
      let popup, lastId, data;
      el.addEventListener('click', function (e) {

        if(!that.mapPopups[marker.properties['id_commessa'] + '_' + marker.properties['id']]){

          that.videowallService.loading.next(true);
          setTimeout(() => {
            popup = new mapboxgl
              .Popup({className: ' popup ', closeOnClick: false})
              .setLngLat(new mapboxgl.LngLat(marker.geometry.coordinates[1], marker.geometry.coordinates[0]))
              .setHTML('<div id="dynamicComponentContainer_' + index + '"></div>')
              .addTo(that.map);
          
              lastId = marker.properties['id_commessa'] + '_' + marker.properties['id'];
              data = {ana: marker.properties};
              that.createDynamicPopup(index, data);
              that.mapPopups[marker.properties['id_commessa'] + '_' + marker.properties['id']] = popup;

              popup.on('close', function(e) {
                delete that.mapPopups[marker.properties['id_commessa'] + '_' + marker.properties['id']];
              })

          }, 200);
        }
      });

    //   el.addEventListener('mouseleave', function (e) { 
    //     setTimeout(() => {
    //       if (that.mapPopups[marker.properties['asset_id']]){
    //         that.mapPopups[marker.properties['asset_id']].remove();
    //         delete that.mapPopups[marker.properties['asset_id']]; 
    //         // that.ipervisioneService.resetSelectedDevice();
    //       }  
    //     }, 200);
    //   });

      let marker2 = new mapboxgl.Marker(el)
        .setLngLat(new mapboxgl.LngLat(marker.geometry.coordinates[1], marker.geometry.coordinates[0]))
        .addTo(that.map);         
      that.mapMarkers.push(marker2)

      var elementsToInject = document.querySelectorAll('.' + markerClass); 
      const size = (marker.properties.squadre.colore == '#ff0000' ? '25px' : '18px');       
      SVGInjector(elementsToInject, { afterAll(elementsLoaded) { }, afterEach(err, svg) { if (err) {throw err;} }, beforeEach(svg) {
        svg.setAttribute('fill', marker.properties.squadre.colore);
        svg.setAttribute('height', size);
        svg.setAttribute('width', size);
      },cacheRequests: false, evalScripts: 'once', httpRequestWithCredentials: false, renumerateIRIElements: false,
      })
    });
  }










  
  // ################################### REMOVES ALL MARKERS ################################### 
  // ################################### REMOVES ALL MARKERS ################################### 
  public removeAllMarkers() {
      if(this.map){

        let allMarkers = document.querySelectorAll('.marker');
        allMarkers.forEach(marker => {
          // console.log('-- : ', m2sarker);   
          marker.remove();
        });
                  
        this.mapMarkers.forEach((marker, index) => {marker.remove();});
        this.mapMarkers = [];
        
        // console.log('----------- REMOVING PIN ----------- : ');   
        
        // for(const key in zones){
        //   // console.log('--- => ', key, ' => ', zones[key]['name']);
        //   if(this.map.getSource(zones[key]['name'] + '_source')){
        //     this.map.getSource(zones[key]['name'] + '_source').setData(empty);
        //   }
        // }
      }
  }
  
  
    // ################################### CREATE DYNAMIC POPUP ################################### 
  // ################################### CREATE DYNAMIC POPUP ################################### 
  public async createDynamicPopup(index, data) {

    const imp = data.ana.wbs + data.ana.rif_impianto
    // console.log('createDynamicPopup => ', data);

    let metrics = await this.videowallService.getSuivi(imp);
    let temps = await this.videowallService.getTemps(imp);
    let compFactory = this.resolver.resolveComponentFactory(CardComponent);
    //if(this.compRef) this.compRef.destroy();
    this.compRef = compFactory.create(this.injector);

    data['ana']['metrics'] = metrics;
    data['ana']['temps'] = temps;
    console.log('CREATE POPUP => ', index, ' : ', data['ana']);

    this.compRef.instance.dataDynamic = data;
    this.compRef.instance.timestamp = this.realtimeTime;
    let div = document.getElementById('dynamicComponentContainer_' + index);
    div.appendChild(this.compRef.location.nativeElement);
    this.compRef.changeDetectorRef.detectChanges();
    this.videowallService.loading.next(false);

  }


  // // ################################### CREATES ZONE LAYER ################################### 
  // // ################################### CREATES ZONE LAYER ################################### 
  // public async createZoneLayer(){
    
  //     let name = 'impianti',
  //       that = this, 
  //       hoveredElement = {}, 
  //       clickedElement = {}, 
  //       id = {};
      
  //     this.map.on('load', async function () {

  //       //########################## ADDING IMAGE, SOURCE AND LAYERS  #################################
  //       //########################## ADDING IMAGE, SOURCE AND LAYERS  #################################
  //       let img = new Image(20,20)
  //       img.onload = ()=>map.addImage('bus', img)
  //       img.src = mySVG
  //       that.map.loadImage('https://sump.aws.engie.it/images/' + name + '.png',(error, image) => {
  //       that.map.loadImage(pin, (error, image) => {
  //         if (error) throw error;
  //         that.map.addImage(name, image);
  //         that.map.addSource(name +'_source', jsonCluster);
  //         that.map.addLayer({ id: name + '_clusters', type: 'circle', source: name + '_source', filter: ['has', 'point_count'], paint: {'circle-color': ['step', ['get', 'point_count'], "#ff0000", 100, "#ff0000", 750, "#ff0000"], 'circle-radius': ['step', ['get', 'point_count'], 15, 100, 25, 750, 35]}});
  //         that.map.addLayer({id: name + '_cluster-count', type: 'symbol', source: name + '_source', filter: ['has', 'point_count'],layout: { 'text-field': '{point_count_abbreviated}', 'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'], 'text-size': 12}, paint: {"text-color": "#ffffff"}});    
  //         that.map.addLayer({id: name + '_unclustered-symbol', type: 'symbol', source: name + '_source', filter: ['!', ['has', 'point_count']], layout: {'icon-image': name, 'icon-size': 0.6,'icon-allow-overlap': true}, paint: {}});
  //       });

  //       //########################## MOUSE CLICK ICONS TO SIMPLIFY #################################
  //       //########################## MOUSE CLICK ICONS TO SIMPLIFY #################################
  //       //CLUSTERED SYMBOLS CLICKS
  //       that.map.on('click', name + '_clusters', (e) => {
  //         let zoom = (that.map.getZoom() < 14 ? 14 : 17);
  //         that.map.setZoom(zoom);
  //         that.map.setCenter(new mapboxgl.LngLat(e.lngLat['lng'], e.lngLat['lat']));
  //       });
          
  //       //UNCLUSTERED SYMBOLS CLICKS
  //       let popup, lastId, data, marker;
  //       that.map.on('click', name + '_unclustered-symbol', (e) => {
  //         marker = e.features[0];
  //         lastId = marker.properties['id_impianto'];
  //         data = {ana: marker.properties};
  //         popup = new mapboxgl.Popup({className: 'popup', closeOnClick: false}).setLngLat(new mapboxgl.LngLat(marker.geometry.coordinates[0], marker.geometry.coordinates[1])).setHTML('<div id="dynamicComponentContainer_' + lastId + '"></div>').addTo(that.map);
  //         that.createDynamicPopup(lastId, data);
  //         that.mapPopups[lastId] = popup;      
  //       });

  //       // UNCLUSTERED SYMBOLS MOVE
  //       // let popup, lastId;
  //       // that.map.on('mousemove', name + '_unclustered-symbol', function (e) {
  //       //   const marker = e.features[0];
  //       //   lastId = marker.properties['asset_id'];
  //       //   if(that.mapPopups[lastId] === undefined && that.mapPopups2[lastId] === undefined){  

  //       //     let temp4 = (that.realtime[lastId]? that.realtime[lastId]:marker.properties);
  //       //     temp4['asset_id'] = temp4['idAsset'];
  //       //     let data2 = {geo: marker.properties, device: temp4};
  //       //     popup = new mapboxgl.Popup({className: 'popup', closeOnClick: false}).setLngLat(new mapboxgl.LngLat(marker.geometry.coordinates[0], marker.geometry.coordinates[1])).setHTML('<div id="dynamicComponentContainer_' + lastId + '"></div>').addTo(that.map);
  //       //     that.createDynamicPopup(lastId, data2);
  //       //     that.mapPopups[lastId] = popup;
  //       //   }
  //       // });
  //       // that.map.on('mouseleave', name + '_unclustered-symbol', function (e) {
  //       //   for(const key in that.mapPopups){
  //       //     if(that.mapPopups[key]){that.mapPopups[key].remove();}
  //       //     delete that.mapPopups[key];
  //       //   }
  //       // });
       
  //   });
  //   return null;
  // }


  // // ################################### UPDATE ZONE LAYER ################################### 
  // // ################################### UPDATE ZONE LAYER ################################### 
  // public inverseCoordinates(data){
  //   data.features.forEach(function (el) {  
  //     return el.geometry.coordinates = el.geometry.coordinates.reverse();
  //   });
  //     return data;
  // }  


  // // ################################### UPDATE ZONE LAYER ################################### 
  // // ################################### UPDATE ZONE LAYER ################################### 
  // public updateClusterLayer(type, data){
  //   let points = JSON.parse(JSON.stringify(jsonCluster));
  //   data.features.forEach(function (el) {el.geometry.coordinates = el.geometry.coordinates.reverse();});
  //   points.features = data.features;
  //   setTimeout(() => {
  //     this.map.getSource(type + '_source').setData(points);
  //   }, (this.map.getSource(type + '_source') ? 0 : 2000));
  // }  



  // ################################### DESTROYER ################################### 
  // ################################### DESTROYER ################################### 
  unsubscribeAll() {
    if(this.darkMode$){this.darkMode$.unsubscribe();}
    if(this.filteredImpianti$){this.filteredImpianti$.unsubscribe();}
  }

  ngOnDestroy() {
    this.unsubscribeAll();
  }

}


