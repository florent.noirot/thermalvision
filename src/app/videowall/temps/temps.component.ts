import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommunicationService } from '../../_services/communication.service';
import { Subscription } from 'rxjs';
import { VideowallService } from '../../_services/videowall.service';

@Component({
  selector: 'temps',
  templateUrl: './temps.component.html',
  styleUrls: ['./temps.component.css']
})
export class TempsComponent implements OnInit, OnDestroy {

  public temps$: Subscription;
  public temps = [];
  public tempTemps = [];
  public tenant$: Subscription;
  public tenant = {};

  constructor(
    public cs: CommunicationService,
    public videowallService: VideowallService,
  ) {
  }

  ngOnInit(): void {
    this.tenant$ = this.videowallService.tenant$.subscribe(async tenant => {
      this.tenant =  tenant;
    });
    
    // SUBSCRIBING TO ALERTS OBSERVABLE FOR NEW ELEMENT RECEIVED THROUGH WEBSOCKET
    this.temps$ = this.cs.temps$.subscribe(data => {
      if (data){
        if (data['tenant'].toLowerCase() == this.tenant['city'].toLowerCase()){
          console.log('++++ => ', data);
          
          this.tempTemps.push(data);
          // console.log('temps 2 => ', this.tempTemps);
          this.temps = JSON.parse(JSON.stringify(this.tempTemps));
          this.temps.reverse();
        }
      }
    });
  }

  ngOnDestroy(){
    this.temps$.unsubscribe();
    this.tenant$.unsubscribe();
  }
}
