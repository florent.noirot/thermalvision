import { Component, AfterViewInit, OnChanges, Input, EventEmitter, Output } from '@angular/core';
import { SVGInjector } from '@tanem/svg-injector'
import { Router, ActivatedRoute } from '@angular/router';
import * as config from '../../../assets/data/config.js';


@Component({ selector: 'top-element', templateUrl: './top-element.component.html', styleUrls: ['./top-element.component.css']})
export class TopElementComponent implements OnChanges, AfterViewInit {

  @Input("data") data;
  @Input("button") button;
  @Output() result = new EventEmitter();
  public ana;
  public fields;
  
  // ################################### CONSTRUCTOR ###################################
  // ################################### CONSTRUCTOR ###################################
  constructor(private router: Router) {}

  ngOnChanges(): void {
    this.fields = config.POPUP_DETAIL_FIELDS['popup'];
    this.ana = this.data.ana;


    if (this.ana.temps){
      console.log('TOP DATA => ', this.ana.temps);
      if (typeof this.ana.temps.value == 'string'){
          this.ana.temps.value = {'temp' : {value: this.ana.temps.value}};
          console.log('XXXX => ', this.ana.temps.value);
        }
      }

  }

  ngAfterViewInit(){
  //  this.createIcone();
  }

  // ################################### CREATE ICONE ###################################
  // ################################### CREATE ICONE ###################################
  public createIcone(){
    var that = this; 
    var elementsToInject = document.querySelectorAll('.marker_main_' + this.ana.id_impianto);          
    SVGInjector(elementsToInject, {
      afterAll(elementsLoaded) {
        // console.log(`injected ${elementsLoaded} elements`);
      },
      afterEach(err, svg) {
        if (err) {throw err;}
        // console.log(`injected ${svg.outerHTML}`)
      },
      beforeEach(svg) {
        svg.setAttribute('fill', this.ana.squadra.colore);
        svg.setAttribute('height', '25px');
        svg.setAttribute('width', '25px');
      },
      cacheRequests: false, evalScripts: 'once', httpRequestWithCredentials: false, renumerateIRIElements: false,
    })
  
  
  }

  // ################################### DESTRUCTOR ###################################
  // ################################### DESTRUCTOR ###################################
  ngOnDestroy() {}
  }
