import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapsComponent } from './maps/maps.component';
import { VideowallManagerComponent } from './videowall-manager/videowall-manager.component';
import { CityComponent } from './city/city.component';
import { AlertsComponent } from './alerts/alerts.component';
import { TasksComponent } from './tasks/tasks.component';
import { Tasks2Component } from '../tasks/tasks/tasks.component';
import { AlertComponent } from './alert/alert.component';
import { TaskComponent } from './task/task.component';
import { TempsComponent } from './temps/temps.component';
import { TempComponent } from './temp/temp.component';
import { SharedModule } from '../shared/shared.module';
import { TickerComponent } from './ticker/ticker.component';
import { CardComponent } from './card/card.component';
import { TopElementComponent } from './top-element/top-element.component';
import { BottomElementComponent } from './bottom-element/bottom-element.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    MapsComponent,
    VideowallManagerComponent,
    CityComponent,
    AlertsComponent,
    TasksComponent,
    AlertComponent,
    TaskComponent,
    TempsComponent,
    TempComponent,
    TickerComponent,
    CardComponent,
    TopElementComponent,
    BottomElementComponent,
    SearchComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    MapsComponent,
    VideowallManagerComponent,
    AlertsComponent,
    TasksComponent,
    CityComponent
  ],
  entryComponents: [Tasks2Component],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class VideowallModule { }
