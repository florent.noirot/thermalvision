// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url:'https://api-thermalvision.noprod.aws.engie.it/noprod',
  url_ws : "wss://wss-thermalvision.noprod.aws.engie.it/noprod",
  key_ws: 'GyW8UsIDcS95KbAen9lBd7OVB9A3PaKP8gdcKrGx',
  code:'',
  
  mapbox: {
    accessToken: 'pk.eyJ1IjoidHo2MjMxIiwiYSI6ImNrb21sYjdpdjBlZDIzMWw2aHBodTl0dGUifQ.tOtBm2Ekv0HWrOcScxZ9hg'
  },
  okta: {
      clientId: '0oa4327yosEUGsa3M0x7',
      issuer: 'https://login.noprod.aws.engie.it/oauth2/aus4327gsiUgRL8xK0x7',
      redirectUri: 'http://' + window.location.host + '/login/callback',
      scopes: 'openid profile email offline_access'.split(/\s+/)
  },
  cookie:{
    cookie: { domain: 'localhost' }, position: "bottom", theme: "classic",
    palette: { popup: { background: "#005288", text: "#ffffff", link: "#ffffff" }, button: { background: "#009de9", text: "#ffffff", border: "transparent" } },
    type: "info",
    content: { message: "Cliccando su “Accetta tutti i cookie”, l'utente accetta di memorizzare i cookie sul dispositivo per migliorare la navigazione del sito, analizzare l'utilizzo del sito e assistere nelle nostre attività di marketing.", dismiss: "Accetta tutti i cookie", deny: "Refuse cookies", link: "Cookie Policy", href: "/cookies", policy: "Cookie Policy" }
  }


};

