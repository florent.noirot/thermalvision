export const environment = {
  production: true,
  //url:'https://api-thermalvision.noprod.aws.engie.it/prod',
  url: 'https://api-thermalvision.aws.engie.it/prod',
  url_ws : "wss://wss-thermalvision.aws.engie.it/prod",
  key_ws: 'GyW8UsIDcS95KbAen9lBd7OVB9A3PaKP8gdcKrGx',
  code: '',
  mapbox: {
    accessToken: 'pk.eyJ1IjoidHo2MjMxIiwiYSI6ImNrb21sYjdpdjBlZDIzMWw2aHBodTl0dGUifQ.tOtBm2Ekv0HWrOcScxZ9hg'
  },
  okta: {
      clientId: '0oa4vdkjc4u5twejH417',
      issuer: 'https://login.aws.engie.it/oauth2/aus4vdl903lTHJFce417',
      redirectUri: 'https://thermalvision.aws.engie.it/login/callback',
      scopes: 'openid profile email offline_access'.split(/\s+/)
  },
};
