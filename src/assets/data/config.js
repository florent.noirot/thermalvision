export const TASK_EMPTY_TEMPLATE = {
  "impianto": {
    "id_impianto": '',
    "nome_impianto": '',
    "priority": 1,
  },
  "cliente": {
    "cliente_nome": "",
    "cliente_cognome": "",
    "cliente_telefono": "",
  },
  "segnalazione": {
    "data_val": "",
    "data_all": "",
    "data_validazione": "",
    "data_allarme": "",
    "task_type": "Segnalazione - Altro",
    "note": ""
  },
  "extra": {
    "tenant": "",
    "nome_utente": "",
  },
};

export const TASK_TYPES = [
  { "value": "Accensione o spegnimento in deroga", "label": "Accensione o spegnimento in deroga" },
  { "value": "Accompagnare Ns. tecnico esterno", "label": "Accompagnare Ns. tecnico esterno" },
  { "value": "Allagamento", "label": "Allagamento" },
  { "value": "Aprire C.T. a personale esterno del condominio", "label": "Aprire C.T. a personale esterno del condominio" },
  { "value": "Cambio orario", "label": "Cambio orario" },
  { "value": "Compilazione libretto impianto", "label": "Compilazione libretto impianto" },
  { "value": "Fermo Impianto", "label": "Fermo Impianto" },
  { "value": "Fuga Gas", "label": "Fuga Gas" },
  { "value": "INTERVENTO IN REPERIBILITA' NON URGENTE (TO)", "label": "Intervento inreperibilita' non urgente" },
  { "value": "Lavoro da cantiere", "label": "Lavoro da cantiere" },
  { "value": "Lavoro preventivato o a consuntivo", "label": "Lavoro preventivato o a consuntivo" },
  { "value": "MANCANZA ACQUA CALDA SANITARIA", "label": "Mancanza acqua calda sanitaria" },
  { "value": "Mancanza acqua calda sanitaria (RM)", "label": "Mancanza acqua calda sanitaria (RM)" },
  { "value": "Perdita Generica", "label": "Perdita Generica" },
  { "value": "Prodotti trattamento acqua e verifica autoclave", "label": "Prodotti trattamento acqua e verifica autoclave" },
  { "value": "Pulizia e messa a riposo", "label": "Pulizia e messa a riposo" },
  { "value": "Richiesta intervento manutenzione - P2", "label": "Richiesta intervento manutenzione - P2" },
  { "value": "Scarico - Ricarico Impianto", "label": "Scarico - Ricarico Impianto" },
  { "value": "Segnalazione regolatore termostatico", "label": "Segnalazione regolatore termostatico" },
  { "value": "Segnalazione ripartitore di calore", "label": "Segnalazione ripartitore di calore" },
  { "value": "Segnalazione - Altro", "label": "Segnalazione - Altro" },
  { "value": "Sfiato Aria", "label": "Sfiato Aria" },
  { "value": "Sopralluogo", "label": "Sopralluogo" },
  { "value": "Termosifoni Freddi/Tiepidi", "label": "Termosifoni Freddi/Tiepidi" },
  { "value": "Verifica corretto funzionamento", "label": "Verifica corretto funzionamento" },
  { "value": "Verifica in appartamento", "label": "Verifica in appartamento" },
];

export const TENANTS = [
  { "city": "", "label": "Tutte le zone", "coordinates": [42.344546, 14.100139], "zoom": 5 },
  { "city": "adriatica", "label": "Adriatica", "coordinates": [42.344546, 14.100139], "zoom": 6 },
  { "city": "lazio", "label": "Lazio", "coordinates": [41.890270, 12.483141], "zoom": 6 },
  { "city": "lombardia_est", "label": "Lombardia Est", "coordinates": [45.548828, 10.119829], "zoom": 6 },
  { "city": "lombardia_ovest", "label": "Lombardia Ovest", "coordinates": [45.452580, 9.182135], "zoom": 6 },
  { "city": "novara", "label": "Novara", "coordinates": [45.449007, 8.621960], "zoom": 6 },
  { "city": "piemonte", "label": "Piemonte", "coordinates": [45.068350, 7.671397], "zoom": 6 },
  { "city": "triveneto", "label": "Triveneto", "coordinates": [45.464112, 11.682973], "zoom": 6 },
];

export const TAB_TYPE = {
  "commessa": { "type": "standard", "reset": true },
  "amministratori": { "type": "list", "reset": true },
  "squadra": { "type": "standard", "reset": true },
  "contatori": { "type": "list", "reset": true },
  "centrali_termiche": { "type": "list", "reset": true },
  "impianti": { "type": "list", "reset": true },
  "ripartitori": { "type": "list", "reset": true },
  "contatermie": { "type": "list", "reset": true },
  "riferimenti_tecnici": { "type": "standard", "reset": true },
  "servizi_impianti": { "type": "standard", "reset": true },
  "eventi_impianti": { "type": "standard", "reset": true },
  "reperibilita": { "type": "standard", "reset": true },
}

export const TABS = [
  "commessa",
  "amministratori",
  // "amministratore", 
  "squadra",
  "contatori",
  "centrali_termiche",
  "impianti",
  "ripartitori",
  "contatermie",
  "riferimenti_tecnici",
  "servizi_impianti",
  "eventi_impianti",
  "reperibilita"
]

export const COMMESSA_LIST_COLUMNS = {
  "col": ["wbs", "assistente_commessa", "sede", "indirizzo", "comune", "tipo_contratto", "scadenza_contratto"],
  "display": {
    "wbs": "CONTRATTO",
    "assistente_commessa": "Assistente",
    "sede": "Sede",
    "indirizzo": "Indirizzo",
    "comune": "Comune",
    "tipo_contratto": "Tipo",
    "scadenza_contratto": "Scadenza"
  }
}

export const COMMESSA_LIST_COLUMNS_FULL = [
  "wbs",
  "id_contratto",
  "importo_contratto",
  "ct_quartieri",
  "conteggio_centrali_termiche",
  "stato",
  "service_leader",
  "assistente_commessa",
  "tecnico_di_zona",
  "commerciale",
  "tenant",
  // "id_amministratore",
  // "nome",
  // "indirizzo",
  // "cap",
  // "citta",
  // "telefono",
  // "email_1",
  // "email_2",
  "tipo_contratto",
  "contratto",
  "opzione_contratto",
  "anno_partenza_plus",
  "convivium",
  "gg_contratto",
  "scad_disdetta",
  "rinnovo_aut",
  "scadenza_contratto",
  "3_resp",
  "global_serv",
  "anticendio",
  "cdz",
  "acs",
  "id_squadra",
  "nome_squadra",
  "colore"
]

export const COMMESSA_DETAIL_FIELDS = {

  "commessa": [
    ["wbs", "Contratto", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : 3, 'maxlength' : 5 }}], // VISIBLE, EDITABLE
    ["id_contratto", "ID Contratto", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : 5 }}],
    ["importo_contratto", "Importo Contratto", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["ct_quartieri", "CT Quartieri", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["conteggio_centrali_termiche", "Conteggio Centrali", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["stato", "Stato", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["service_leader", "Service Leader", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["assistente_commessa", "Assistente Commessa", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["tecnico_di_zona", "Tecnico Di Zona", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["commerciale", "Commerciale", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["id_tenant", "ID Tenant", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["tipo_contratto", "Tipo Contratto", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["contratto", "Contratto", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["opzione_contratto", "Opzione Contratto", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["anno_partenza_plus", "Anno Partenza +", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["convivium", "Convivium", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["gg_contratto", "GG Contratto", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["scad_disdetta", "Scad Disdetta", "input", "true", "true", "date", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["rinnovo_aut", "Rinnovo Auto", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["scadenza_contratto", "Scadenza Contratto", "input", "true", "true", "date", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["3_resp", "3 Resp", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["global_serv", "Global Serv", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["anticendio", "Anticendio", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}]
  ],

  "amministratori": [
    ["id", "ID Amministratore", "input", "true", "true", "text", {'error' : {'required' : true, 'minlength' : false, 'maxlength' : false }}],
    ["nome", "Nome", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["indirizzo", "Indirizzo", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["cap", "Cap", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["citta", "Citta", "input", "true", "true", "text", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["telefono", "Telefono", "input", "true", "true", "number", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["email_1", "Email 1", "input", "true", "true", "email", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}],
    ["email_2", "Email 2", "input", "true", "true", "email", {'error' : {'required' : 'true', 'minlength' : false, 'maxlength' : false }}]
  ],

  "amministratore": [
    ["id", "ID Amministratore", "input", "true", "true", "text", {'error' : {'required' : true, 'minlength' : false, 'maxlength' : false }}],
    ["nome", "Nome", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["indirizzo", "Indirizzo", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["cap", "Cap", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["citta", "Citta", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["telefono", "Telefono", "input", "true", "true", "number", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["email_1", "Email 1", "input", "true", "true", "email", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["email_2", "Email 2", "input", "true", "true", "email", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}]
  ],

  "squadra": [
    ["id", "ID Squadra", "input", "true", "true", "text", {'error' : {'required' : true, 'minlength' : false, 'maxlength' : false }}],
    ["nome", "Nome Squadra", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["colore", "Colore", "input", "true", "true", "color", {'error' : {'required' : 'false', 'minlength' : false, 'maxlength' : false }}]
  ],

  "contatori": [
    ["matricola_contatore_meccanico", "Matricola Contatore Meccanico", "input", "true", "true", "text", {'error' : {'required' : true, 'minlength' : false, 'maxlength' : false }}],
    ["matricola_contatore_volumetrico", "Matricola Contatore Volumetrico", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["pdr", "PDR", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["fornitore", "Fornitore", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["id_impianto", "ID Impianto", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}]
  ],

  "centrali_termiche": [
    ["id_centrale", "ID Centrale", "input", "true", "true", "text", {'error' : {'required' : true, 'minlength' : false, 'maxlength' : false }}],
    ["marca_generatore", "Marca Generatore", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["modello_generatore", "Modello Generatore", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["n_cald", "N CALD", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["pot_foc_kw", "Potenteza Foc", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["pot_utile_kw", "Potenteza Utile", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["fludio_termov", "Fludio Termov", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["data_istallazione", "Data Istallazione", "input", "true", "true", "date", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["combustibile", "Combustibile", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["condizione", "Condizione", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["scad_smart_box", "Scad Smartbox", "input", "true", "true", "date", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["riferimento_in_loco_chiavi", "rif in loco chiavi", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["id_impianto", "ID Impianto", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}]
  ],

  "impianti": [
    ["id_impianto", "ID Impianto", "input", "true", "true", "text", {'error' : {'required' : true, 'minlength' : false, 'maxlength' : false }}],
    ["wbs", "WBS", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["sede", "Sede", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["indirizzo", "Indirizzo", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["comune", "Comune", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["lat", "Lat", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["lon", "Lon", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["marca_telecontrollo", "Marca Telecontrollo", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["modello_telecontrollo", "Modello Telecontrollo", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["#SIM", "#SIM", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["funzionamento_estivo", "Funzionamento Estivo", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["cdz", "CDZ", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["acs", "ACS", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}]
  ],

  "ripartitori": [
    ["id_ripartitore", "ID Ripartitore", "input", "true", "true", "text", {'error' : {'required' : true, 'minlength' : false, 'maxlength' : false }}],
    ["fornitore", "Fornitore", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["tipo_fattura", "Tipo Fattura", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["costo_lettura_cliente", "Costo Lettura Cliente", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["numero_reale", "Numero Reale", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["anno_installazione", "Anno Installazione", "input", "true", "true", "date", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["lettura", "Lettura", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["ripartizione", "Ripartizione", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["uni_10200", "UNI 10200", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["id_impianto", "ID Impianto", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
  ],
  "contatermie": [
    ["id_impianto", "ID Impianto", "input", "true", "true", "text", {'error' : {'required' : true, 'minlength' : false, 'maxlength' : false }}],
    ["contatermie", "Contatermie", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["censito", "Censito", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["marca", "Marca", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["modello", "Modello", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["DN", "DN", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["anno_marcatura_stumento", "Anno Marcatura Stumento", "input", "true", "true", "date", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["anno_messa_in_servizio_strumento", "Anno Messa In Servizio Strumento", "input", "true", "true", "date", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
  ],

  "riferimenti_tecnici": [
    ["id_riferimento", "ID Riferimento", "input", "true", "true", "text", {'error' : {'required' : true, 'minlength' : false, 'maxlength' : false }}],
    ["nome", "Nome", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["tipo", "Tipo", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["zona", "Zona", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["telefono_1", "Telefono 1", "input", "true", "true", "number", {'error' : {'required' : true, 'minlength' : false, 'maxlength' : false }}],
    ["telefono_2", "Telefono 2", "input", "true", "true", "number", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["telefono_3", "Telefono 3", "input", "true", "true", "number", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}]
  ],
  "servizi_impianti": [
    ["id_impianto", "ID Impianto", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["tipo_servizio", "Tipo Servizio", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["tipologia_contratto", "Tipologia Contratto", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["marca", "Marca", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["modello", "Modello", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}]
  ],
  "eventi_impianti": [
    ["id_impianto", "ID Impianto", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["evento", "Evento", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["data_evento", "Data Evento", "input", "true", "true", "date", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["ore", "Ore", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["note", "Note", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["fascia_climatica", "Fascia Climatica", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}]
  ],
  "reperibilita": [
    ["id_impianto", "ID Impianto", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["tipo_reperibilita", "Tipo Reperibilita", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["telefono", "Telefono", "input", "true", "true", "number", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["disponibilita_standard", "Disponibilita Standard", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["note", "Note", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}]
  ],
  "impianto": [
    ["id_impianto", "Sede Technica", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["nome_impianto", "Nome Impianto", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
  ],
  "cliente": [
    ["cliente_nome", "Nome", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["cliente_cognome", "Cognome", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["cliente_telefono", "Telefono", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}]
  ],
  "segnalazione": [
    ["data_val", "Data Validazione", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["data_all", "Data Allarme", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["priority", "Criticità", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["task_type", "Tipo Task", "input", "true", "true", "select", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}],
    ["note", "Note", "input", "true", "true", "text", {'error' : {'required' : false, 'minlength' : false, 'maxlength' : false }}]
  ]
}

export const POPUP_DETAIL_FIELDS = {
  "popup": [
    ["wbs", "Impianto", "display", "false", "false"], // VISIBLE, EDITABLE
    ["indirizzo", "Indirizzo", "display", "false", "false"],
    ["nome", "Squadra", "display", "false", "false"],
  ]
}

export const TASK_LIST_COLUMNS = {
  "col": ["id", "cliente_nome", "cliente_cognome", "status", "task_type", "open_date"],
  "display": {
    "id": "ODL",
    "cliente_nome": "Nome",
    "cliente_cognome": "Cognome",
    "status": "Stato",
    // "cliente_telefono": "Telefono",
    "task_type": "Type",
    "open_date": "Validazione"
  }
}

export const TASK_LIST_COLUMNS_FULL = [
  "id",
  "sede",
  "indirizzo",
  "status",
  "priority",
  "task_type",
  "odl",
  "data_validazione",
  "data_allarme",
  "action",
  "note"
]

export const TASK_TABS = [
  "impianto",
  "cliente",
  "segnalazione"
]

export const TASK_TAB_TYPE = {
  "impianto": { "type": "standard", "reset": false },
  "cliente": { "type": "standard", "reset": false },
  "segnalazione": { "type": "standard", "reset": false }
}


export const ALARMS_LIST_COLUMNS = {
  "col": ["timestamp", "id_impianto", "nome_impianto", "indirizzo", "comune", "alarm_name", "action"],
  "display": {
    "timestamp": "Data",
    "id_impianto": "ID",
    "nome_impianto": "Nome",
    "indirizzo": "Indirizzo",
    "comune": "Comune",
    "alarm_name": "Code",
    "action": "Action"
  }
}

export const ALARM_LIST_COLUMNS_FULL = [
  "tenant",
  "value",
  "id_device",
  "id_impianto",
  "alarm_name",
  "wbs",
  "timestamp",
  "id_alarm",
  "nome_impianto",
  "rif_impianto",
  "id_amministratore",
  "sede",
  "indirizzo",
  "comune"
]