export const ALERTS = [
  {
    action: "sendmessage",
    data: {"type": "alarm", "id_alarm": 22, "tenant":"lombardia_est", "wbs": "0047075", "rif_impianto": "001", "id_impianto": 12, "id_device": 33, "timestamp": 1658687189, "alarm_name": "BLC_GEC"}
  },
  {
    action: "sendmessage",
    data: {"type": "alarm", "id_alarm": 23, "tenant":"lombardia_est", "wbs": "0047076", "rif_impianto": "001", "id_impianto": 13, "id_device": 34, "timestamp": 1658676189, "alarm_name": "BLC_GEC"}
  },
  {
    action: "sendmessage",
    data: {"type": "alarm", "id_alarm": 24, "tenant":"lombardia_est", "wbs": "0047077", "rif_impianto": "001", "id_impianto": 14, "id_device": 35, "timestamp": 1658665189, "alarm_name": "BLC_GEC"}
  },
  {
    action: "sendmessage",
    data: {"type": "alarm", "id_alarm": 25, "tenant":"lombardia_est", "wbs": "0047078", "rif_impianto": "001", "id_impianto": 15, "id_device": 36, "timestamp": 1658654189, "alarm_name": "BLC_GEC"}
  },
  {
    action: "sendmessage",
    data: {"type": "alarm", "id_alarm": 24, "tenant":"lombardia_est", "wbs": "0047079", "rif_impianto": "001", "id_impianto": 14, "id_device": 35, "timestamp": 1658643189, "alarm_name": "BLC_GEC"}
  },
  {
    action: "sendmessage",
    data: {"type": "alarm", "id_alarm": 25, "tenant":"lombardia_est", "wbs": "0047080", "rif_impianto": "001", "id_impianto": 15, "id_device": 36, "timestamp": 1658632189, "alarm_name": "BLC_GEC"}
  },
  {
    action: "sendmessage",
    data: {"type": "alarm", "id_alarm": 24, "tenant":"lombardia_est", "wbs": "0047081", "rif_impianto": "001", "id_impianto": 14, "id_device": 35, "timestamp": 1658621189, "alarm_name": "BLC_GEC"}
  },
  {
    action: "sendmessage",
    data: {"type": "alarm", "id_alarm": 25, "tenant":"lombardia_est", "wbs": "0047082", "rif_impianto": "001", "id_impianto": 15, "id_device": 36, "timestamp": 1658610189, "alarm_name": "BLC_GEC"}
  },
];

export const TEMPS = [
  // {
  //   action: "sendmessage",
  //   data: {
  //     "tenant": "adriatica","value": {"Sonda_B2": 35.1,"Sonda_B4": 58.6},
  //     "id_device": "200", "id_impianto": "48559002", "wbs": "48559", "timestamp": 1659625366,"source": "coster", "id_temperature": "1", "nome_impianto": "IL PARCO", "rif_impianto": "002" }
  // },
  
  {
    action: "sendmessage",
    data: {"type": "temperature", "tenant":"lombardia_est", "id_temperature": 2, "id_impianto": 53, "wbs": "0047076", "rif_impianto": "001", "value": 46, "timestamp": 1658676189}
  },
  {
    action: "sendmessage",
    data: {"type": "temperature", "tenant":"lombardia_est", "id_temperature": 3, "id_impianto": 54, "wbs": "0047077", "rif_impianto": "001", "value": 47, "timestamp": 1658676189}
  },
  {
    action: "sendmessage",
    data: {"type": "temperature", "tenant":"lombardia_est", "id_temperature": 4, "id_impianto": 5, "wbs": "0047078", "rif_impianto": "001", "value": 48, "timestamp": 1658665189}
  },
  {
    action: "sendmessage",
    data: {"type": "temperature", "tenant":"lombardia_est", "id_temperature": 1, "id_impianto": 52, "wbs": "0047079", "rif_impianto": "001", "value": 49, "timestamp": 1658654189}
  },
  {
    action: "sendmessage",
    data: {"type": "temperature", "tenant":"lombardia_est", "id_temperature": 2, "id_impianto": 53, "wbs": "0047080", "rif_impianto": "001", "value": 50, "timestamp": 1658643189}
  },
  {
    action: "sendmessage",
    data: {"type": "temperature", "tenant":"lombardia_est", "id_temperature": 3, "id_impianto": 54, "wbs": "0047081", "rif_impianto": "001", "value": 51, "timestamp": 1658632189}
  },
  {
    action: "sendmessage",
    data: {"type": "temperature", "tenant":"lombardia_est", "id_temperature": 4, "id_impianto": 5, "wbs": "0047082", "rif_impianto": "001", "value": 52, "timestamp": 1658621189}
  }
];


export const TASKS = [
  {
    action: "sendmessage",
    data: {"type": "task", "id": 1, "open_timestamp": 1658698189,"close_timestamp": "5/6/2022", "tenant": "lombardia_est","status": "new", "priority": 1, "task_type": "task", "cliente_nome": "marco","cliente_cognome": "rossi","cliente_telefono": "123456789","data_validazione_vw": "10/6/2022","data_allarme_vw": "14/6/2022","nome_utente_vw": "Ciro Esposito","tipo_ordine": "A" ,"rf_property_ag": "rf1", "note": "description 1", "wbs": "0047075", "rif_impianto": "001","id_impianto": 12}
  },
  {
    action: "sendmessage",
    data: {"type": "task", "id": 2, "open_timestamp": 1658687189,"close_timestamp": "6/6/2022", "tenant": "lombardia_est","status": "new", "priority": 1, "task_type": "task", "cliente_nome": "marcello","cliente_cognome": "blu","cliente_telefono": "234567891","data_validazione_vw": "11/6/2022","data_allarme_vw": "15/6/2022","nome_utente_vw": "Vito Ferrari","tipo_ordine": "A" ,"rf_property_ag": "rf2", "note": "description 2", "wbs": "0047076", "rif_impianto": "001", "id_impianto": 13}
  },
  {
    action: "sendmessage",
    data: {"type": "task", "id": 3, "open_timestamp": 1658676189,"close_timestamp": "7/6/2022", "tenant": "lombardia_est","status": "new", "priority": 1, "task_type": "task", "cliente_nome": "marcia","cliente_cognome": "verde","cliente_telefono": "345678912","data_validazione_vw": "12/6/2022","data_allarme_vw": "16/6/2022","nome_utente_vw": "Gino Rossi","tipo_ordine": "A" ,"rf_property_ag": "rf3", "note": "description 3", "wbs": "0047077", "rif_impianto": "001", "id_impianto": 14}
  },
  {
    action: "sendmessage",
    data: {"type": "task", "id": 4, "open_timestamp": 1658665189,"close_timestamp": "8/6/2022", "tenant": "lombardia_est","status": "new", "priority": 1, "task_type": "task", "cliente_nome": "marc","cliente_cognome": "giallo","cliente_telefono": "456789123","data_validazione_vw": "13/6/2022","data_allarme_vw": "17/6/2022","nome_utente_vw": "Gianni Verde","tipo_ordine": "A" ,"rf_property_ag": "rf4", "note": "description 4", "wbs": "0047078", "rif_impianto": "001", "id_impianto": 15}
  },
  {
    action: "sendmessage",
    data: {"type": "task", "id": 5, "open_timestamp": 1658654189,"close_timestamp": "9/6/2022", "tenant": "lombardia_est","status": "new", "priority": 1, "task_type": "task", "cliente_nome": "marco","cliente_cognome": "rossi","cliente_telefono": "123456789","data_validazione_vw": "10/6/2022","data_allarme_vw": "14/6/2022","nome_utente_vw": "Ciro Esposito","tipo_ordine": "A" ,"rf_property_ag": "rf1", "note": "description 1", "wbs": "0047079", "rif_impianto": "001","id_impianto": 12}
  },
  {
    action: "sendmessage",
    data: {"type": "task", "id": 6, "open_timestamp": 1658643189,"close_timestamp": "6/6/2022", "tenant": "lombardia_est","status": "new", "priority": 1, "task_type": "task", "cliente_nome": "marcello","cliente_cognome": "blu","cliente_telefono": "234567891","data_validazione_vw": "11/6/2022","data_allarme_vw": "15/6/2022","nome_utente_vw": "Vito Ferrari","tipo_ordine": "A" ,"rf_property_ag": "rf2", "note": "description 2", "wbs": "0047080", "rif_impianto": "001", "id_impianto": 13}
  },
  {
    action: "sendmessage",
    data: {"type": "task", "id": 7, "open_timestamp": 1658632189,"close_timestamp": "7/6/2022", "tenant": "lombardia_est","status": "new", "priority": 1, "task_type": "task", "cliente_nome": "marcia","cliente_cognome": "verde","cliente_telefono": "345678912","data_validazione_vw": "12/6/2022","data_allarme_vw": "16/6/2022","nome_utente_vw": "Gino Rossi","tipo_ordine": "A" ,"rf_property_ag": "rf3", "note": "description 3", "wbs": "0047081", "rif_impianto": "001", "id_impianto": 14}
  },
  {
    action: "sendmessage",
    data: {"type": "task", "id": 8, "open_timestamp": 1658621189,"close_timestamp": "8/6/2022", "tenant": "lombardia_est","status": "new", "priority": 1, "task_type": "task", "cliente_nome": "marc","cliente_cognome": "giallo","cliente_telefono": "456789123","data_validazione_vw": "13/6/2022","data_allarme_vw": "17/6/2022","nome_utente_vw": "Gianni Verde","tipo_ordine": "A" ,"rf_property_ag": "rf4", "note": "description 4", "wbs": "0047082", "rif_impianto": "001", "id_impianto": 15}
  }
];








